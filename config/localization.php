<?php

return [

    /* -----------------------------------------------------------------
     |  Settings
     | -----------------------------------------------------------------
     */

    'supported-locales'      => ['en', 'fr', 'nl'],

    'accept-language-header' => true,

    'hide-default-in-url'    => false,

    'redirection-code'       => 302,

    'utf-8-suffix'           => '.UTF-8',

    /* -----------------------------------------------------------------
     |  Route
     | -----------------------------------------------------------------
     */

    'route'                  => [
        'middleware' => [
            'localization-session-redirect' => true,
            'localization-cookie-redirect'  => false,
            'localization-redirect'         => true,
            'localized-routes'              => true,
            'translation-redirect'          => true,
        ],
    ],

    /* -----------------------------------------------------------------
     |  Ignored URI/Route from localization
     | -----------------------------------------------------------------
     */

    'ignored-redirection' => [
        //
    ],

    /* -----------------------------------------------------------------
     |  Locales
     | -----------------------------------------------------------------
     */

    'locales'   => [
        'en'          => [
            'name'     => 'English',
            'script'   => 'Latn',
            'dir'      => 'ltr',
            'native'   => 'English',
            'regional' => 'en_GB',
        ],
        'fr'          => [
            'name'     => 'French',
            'script'   => 'Latn',
            'dir'      => 'ltr',
            'native'   => 'Français',
            'regional' => 'fr_FR',
        ],
        'nl'          => [
            'name'     => 'Dutch',
            'script'   => 'Latn',
            'dir'      => 'ltr',
            'native'   => 'Nederlands',
            'regional' => 'nl_NL',
        ],
    ],

];
