<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('brand_id')->nullable();
            $table->string('title_nl')->nullable();
            $table->string('slug_nl')->nullable();
            $table->string('title_fr')->nullable();
            $table->string('slug_fr')->nullable();
            $table->string('title_en')->nullable();
            $table->string('slug_en')->nullable();
            $table->string('subtitle_nl')->nullable();
            $table->string('subtitle_fr')->nullable();
            $table->string('subtitle_en')->nullable();
            $table->text('description_nl')->nullable();
            $table->text('description_fr')->nullable();
            $table->text('description_en')->nullable();
            $table->bigInteger('color_id')->nullable();
            $table->string('first_inscription_date')->nullable();
            $table->string('price', 11)->nullable();
            $table->string('horsepower')->nullable();
            $table->string('kilometers')->nullable();
            $table->bigInteger('transmission_id')->nullable();
            $table->bigInteger('fuel_id')->nullable();
            $table->integer('co2')->nullable();
            $table->tinyInteger('is_active');
            $table->tinyInteger('is_vat_vehicle')->default('0');
            $table->tinyInteger('is_sold')->default('0');           
            $table->tinyInteger('has_option')->default('0');
            $table->tinyInteger('is_featured')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
