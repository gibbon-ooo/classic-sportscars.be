<?php

return [
	"search-cars"               => "Zoek wagens...",
    "select-fuel"               => "Kies brandstoftype...",
    "select-brand"              => "Kies merk...",
    "reset-filters"             => "Reset Filters",
    "vat-recoverable"           => "BTW recupereerbaar",
    "pollution-standard"        => "Pollutienorm (Euro)",
    "yes"                       => "Ja",
    "no"                        => "Neen",
    "netto-price"               => "Netto prijs",
    "gross-price"               => "Brutto prijs",
    "vat-excluded"               => "BTW exclusief",



	"home"						=> 	"Welkom",
	"home-title"				=> 	"Welkom",

	"our-vehicles"				=>  "Onze voertuigen",
	"our-vehicles-title"		=>  "Onze voertuigen",

	"who-we-are"				=> 	"Over ons",
	"who-we-are-title"			=> 	"Over ons",

	"buying-and-selling"		=> 	"Aankoop en verkoop",
	"buying-and-selling-title"	=> 	"Aankoop en verkoop",

	"contact"					=> 	"Contact",
	"contact-title"				=> 	"Contact",

	"gallery"					=> 	"Fotogalerij",
	"gallery-title"				=> 	"Fotogalerij",

	"vat-included"				=>	"BTW inclusief",
	"vat-vehicle"				=>  "BTW voertuig",
	"first-registration"		=>  "Eerste inschrijving",
	"color"						=>  "Kleur",
	"mileage"					=>  "Kilometerstand",
	"gearing-type"				=>  "Transmissie",
	"fuel"						=>  "Brandstof",
	"horsepower"				=>  "PK",
	"description"				=>  "Beschrijving",
	"available"					=>  "Beschikbaar",
	"option"					=>  "Optie",
	"sold"						=>  "Verkocht",

	"first-name"				=>  "Voornaam",
	"last-name"					=>  "Achternaam",
	"e-mail"					=>  "E-mailadres",
	"telephone"					=>  "Telefoonnummer",
	"brand"						=>  "Merk - type van uw voertuig",
	"make-a-choice"				=>  "Maak een keuze",
	"description"				=>  "Beschrijving - Opties",
	"website"					=>  "Website van het zoekertje van uw voertuig",
	"file"						=>  "Bestand",
	"send"						=>  "Verzenden",
	"subject"					=>  "Onderwerp",
	"message"					=>  "Bericht",
	"message-sent"				=>  "Je bericht werd goed verzonden.",
	"all-fields"				=>  "Gelieve alle verplichte velden correct in te vullen.",
	"total-stock"				=>  "Stock",

	"sorting"					=> "Sorteer op:",
	"most-recent"				=> "Meest recente voertuigen eerst",
	"highest-price"				=> "Duurste voertuigen eerst",

];
