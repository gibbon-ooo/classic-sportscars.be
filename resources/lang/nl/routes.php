<?php
	
return [
	
	"home"					=> 	"home",
	"our-vehicles"			=>  "aanbod",
	"our-vehicles/car"		=> 	"aanbod/{slug?}",
	"who-we-are"			=> 	"over-ons",
	"buying-and-selling"	=> 	"aankoop",
	"contact"				=> 	"contact",
	"gallery"				=> 	"fotogalerij"
	
];