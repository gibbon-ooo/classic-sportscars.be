<?php
	
return [
	
	"home"						=> 	"Bienvenue",
	"home-title"				=> 	"Bienvenue",
	
	"our-vehicles"				=>  "Nos voitures",
	"our-vehicles-title"		=>  "Nos voitures",
	
	"who-we-are"				=> 	"Qui sommes nous",
	"who-we-are-title"			=> 	"Qui sommes nous",
	
	"buying-and-selling"		=> 	"Achat et vente",
	"buying-and-selling-title"	=> 	"Achat et vente",
	
	"contact"					=> 	"Contactez-nous",
	"contact-title"				=> 	"Contactez-nous",

	"gallery"					=> 	"Galerie photo",
	"gallery-title"				=> 	"Galerie photo",		
	
	"vat-included"				=>	"TVA incluse",
	"vat-vehicle"				=>  "TVA déductible",
	"first-registration"		=>  "Première immatriculation",
	"color"						=>  "Couleur",
	"mileage"					=>  "Kilometrage",
	"gearing-type"				=>  "Transmission",
	"fuel"						=>  "Carburant",
	"horsepower"				=>  "CV",
	"description"				=>  "Description",
	"available"					=>  "Disponible",
	"option"					=>  "Option",
	"sold"						=>  "Vendu",
	
	"first-name"				=>  "Prénom",
	"last-name"					=>  "Nom",
	"e-mail"					=>  "Adresse e-mail",	
	"telephone"					=>  "Téléphone",
	"brand"						=>  "Marque - type de votre véhicule",
	"make-a-choice"				=>  "Faites votre choix",
	"description"				=>  "Déscription - Options",
	"website"					=>  "Annonce de votre véhicule sur le web",
	"file"						=>  "Photo du véhicule",
	"send"						=>  "Envoyer",
	"subject"					=>  "Sujet",
	"message"					=>  "Message",
	"message-sent"				=>  "Votre message a bien été envoyé.",
	"all-fields"				=>  "Veuillez remplir tous les champs requis.",
	"total-stock"				=>  "Stock",

	"sorting"					=> "Trier par:",
	"most-recent"				=> "Annonces les plus récentes d’abord",
	"highest-price"				=> "Prix ordre décroissant",

];