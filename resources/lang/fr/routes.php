<?php
	
return [
	
	"home"					=> 	"accueil",
	"our-vehicles"			=>  "nos-vehicules",
	"our-vehicles/car"		=> 	"nos-vehicules/{slug?}",
	"who-we-are"			=> 	"qui-sommes-nous",
	"buying-and-selling"	=> 	"achat",
	"contact"				=> 	"contact",
	"gallery"				=> 	"galerie-photo"
	
];