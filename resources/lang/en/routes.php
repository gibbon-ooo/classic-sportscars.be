<?php
	
return [
	
	"home"					=> 	"home",
	"our-vehicles"			=>  "our-vehicles",
	"our-vehicles/car"		=> 	"our-vehicles/{slug?}",
	"who-we-are"			=> 	"who-we-are",
	"buying-and-selling"	=> 	"buying-and-selling",
	"contact"				=> 	"contact",
	"gallery"				=> 	"photo-gallery"
	
];