<?php
	
return [
	
	"home"						=> 	"Welcome",
	"home-title"				=> 	"Welcome",

	"our-vehicles"				=>  "our vehicles",
	"our-vehicles-title"		=>  "our vehicles",

	"who-we-are"				=> 	"About us",
	"who-we-are-title"			=> 	"About us",

	"buying-and-selling"		=> 	"Buying & Selling",
	"buying-and-selling-title"	=> 	"Buying & Selling",

	"contact"					=> 	"Contact us",
	"contact-title"				=> 	"Contact us",

	"gallery"					=> 	"Photo Gallery",
	"gallery-title"				=> 	"Photo Gallery",	
		
	"vat-included"				=>	"VAT included",
	"vat-vehicle"				=>  "VAT vehicle",
	"first-registration"		=>  "First registration",
	"color"						=>  "Color",
	"mileage"					=>  "Mileage",
	"gearing-type"				=>  "Gearing type",
	"fuel"						=>  "Fuel",
	"horsepower"				=>  "HP",
	"description"				=>  "Description",
	"available"					=>  "Available",
	"option"					=>  "Option",
	"sold"						=>  "Sold",
	
	"first-name"				=>  "First name",
	"last-name"					=>  "Last name",
	"e-mail"					=>  "Email address",	
	"telephone"					=>  "Phone",
	"brand"						=>  "Brand - type of your vehicle",
	"make-a-choice"				=>  "Please make a choice",
	"description"				=>  "Description - Options",
	"website"					=>  "Url from your ad on the web",
	"file"						=>  "File",
	"send"						=>  "Send",
	"subject"					=>  "Subject",
	"message"					=>  "Message",
	"message-sent"				=>  "Your message has been send successfully.",
	"all-fields"				=>  "Please enter all required fields.",
	"total-stock"				=>  "Stock",

	"sorting"					=> "Sort by:",
	"most-recent"				=> "Most recent offers first",
	"highest-price"				=> "Most exspensive offers first",

];