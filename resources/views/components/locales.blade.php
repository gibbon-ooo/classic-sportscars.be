<div class="absolute top-0 left-0 mt-4 ml-2">
    <div
        x-data="{
            open: false,
            toggle() {
                if (this.open) {
                    return this.close()
                }

                this.open = true
            },
            close(focusAfter) {
                this.open = false

                focusAfter && focusAfter.focus()
            }
        }"
        x-on:keydown.escape.prevent.stop="close($refs.button)"
        x-on:focusin.window="! $refs.panel.contains($event.target) && close()"
        x-id="['dropdown-button']"
        class="relative min-w-12"
    >
        <!-- Button -->
        <button
            x-ref="button"
            x-on:click="toggle()"
            :aria-expanded="open"
            :aria-controls="$id('dropdown-button')"
            type="button"
            class="px-4 py-2 -mt-2"
        >
            <span class="text-white text-2xl font-bold uppercase">{{ localization()->getCurrentLocale() }}</span>
        </button>

        <!-- Panel -->
        <div
            x-ref="panel"
            x-show="open"
            x-transition.origin.top.left
            x-on:click.outside="close($refs.button)"
            :id="$id('dropdown-button')"
            style="display: none;"
            class="absolute left-0 -mt-1"
        >
            <div>
                @foreach(localization()->getSupportedLocalesKeys() as $locale)
                    @if($locale !== localization()->getCurrentLocale())
                        <a rel="alternate" class="block w-full px-4 py-1 text-left text-white text-2xl uppercase" hreflang="{{ $locale }}" href="{{ localization()->getLocalizedURL($locale) }}">
                            {{ $locale }}
                        </a>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>
