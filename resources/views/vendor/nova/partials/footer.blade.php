<p class="mt-8 text-center text-xs text-80">
    Classic and Sportscars Dashbord
    <span class="px-1">&middot;</span>
    &copy; {{ date('Y') }} - <a href="https://gibbon.ooo" class="text-primary dim no-underline">By Gibbon</a>.
    <span class="px-1">&middot;</span>
    v{{ \Laravel\Nova\Nova::version() }}
</p>
