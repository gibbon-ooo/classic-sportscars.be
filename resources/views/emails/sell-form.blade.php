@component('mail::message')

Prénom: {{ $data['firstname'] }}  
Nom: {{ $data['lastname'] }}  

E-mail: {{ $data['email'] }}  
Téléphone: {{ $data['telephone'] }}  

Marque: {{ $data['brand'] }}  

Site web: {{ $data['website'] }}  

Déscription:  
{{ $data['description'] }}

@endcomponent
