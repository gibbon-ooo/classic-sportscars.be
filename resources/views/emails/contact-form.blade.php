@component('mail::message')

Prénom: {{ $data['firstname'] }}  
Nom: {{ $data['lastname'] }}  

E-mail: {{ $data['email'] }}  
Téléphone: {{ $data['telephone'] }}  

Message:  
{{ $data['messageText'] }}

@endcomponent
