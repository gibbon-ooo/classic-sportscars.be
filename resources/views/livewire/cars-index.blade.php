<section class="bg-gray-100 px-4 md:px-8 py-12 md:py-20 border border-gray-200">
        
    <div class="max-w-7xl mx-auto grid gap-6 md:gap-8 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
    @foreach($cars->get() as $car)
        
        <a href="{{ localization()->getUrlFromRouteName(session('locale'), 'routes.our-vehicles/car', $attributes = []) }}/{{ $car->slug }}">

            <div class="rounded-2xl overflow-hidden bg-white shadow">
                <div x-data 
                    x-init="() => {
                                var glide = new Glide($el, {
                                  type: 'carousel',
                                  gap: 0,
                                });
                                glide.mount()
                            }"
                    class="glide"
                >
                    <div class="glide__track" data-glide-el="track">
                        <ul class="glide__slides">
                            @foreach($car->getMedia('photos')->take(5) as $media)
                            <li class="glide__slide">
                                <img src="{{ $media->getUrl('thumb') }}">
                            </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="absolute top-1/2 flex justify-between -mt-6 opacity-60 p-2 w-full glide__arrows" data-glide-el="controls">
                        <button class="glide__arrow glide__arrow--left" data-glide-dir="<">
                            <svg class="w-10 h-10 text-white" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm.707-10.293a1 1 0 00-1.414-1.414l-3 3a1 1 0 000 1.414l3 3a1 1 0 001.414-1.414L9.414 11H13a1 1 0 100-2H9.414l1.293-1.293z" clip-rule="evenodd"></path></svg>
                        </button>
                        <button class="glide__arrow glide__arrow--right" data-glide-dir=">">
                            <svg class="w-10 h-10 text-white" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-8.707l-3-3a1 1 0 00-1.414 1.414L10.586 9H7a1 1 0 100 2h3.586l-1.293 1.293a1 1 0 101.414 1.414l3-3a1 1 0 000-1.414z" clip-rule="evenodd"></path></svg>
                        </button>
                    </div>

                   <div class="w-full absolute bottom-0 glide__bullets" data-glide-el="controls[nav]">
                        @foreach($car->getMedia('photos')->take(5) as $k => $media)
                        <button class="glide__bullet" data-glide-dir="={{ $k }}"></button>
                        @endforeach
                    </div>

                </div>

                <div class="p-4">
                    <h3 class="text-sm font-medium min-h-[50px]">{{ $car->title }}</h3>
                </div>
            </div>
        </a>

    @endforeach
    </div>
</section>