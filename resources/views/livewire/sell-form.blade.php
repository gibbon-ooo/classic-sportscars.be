<div class="pr-0 md:pr-8">
    
    @if (count($errors) > 0)
        <div class="bg-red-100 border-red-200 p-2 rounded text-red-800 mb-8">
            {{ trans('messages.all-fields') }}
        </div>
    @endif
    
    @if (Session::has('status') )
        <div class="bg-green-100 border-green-200 p-2 rounded text-green-800 mb-8">
            {{ Session::get('status') }}
        </div>
    @else

        <form wire:submit.prevent="submit" enctype="multipart/form-data">
      
            <div class="flex flex-col sm:flex-row sm:flex-wrap -mx-4 mb-4">
                <div class="w-full sm:w-1/2 flex flex-col space-y-1 px-4">
                    <label for="firstname" class="">{{ trans('messages.first-name') }} <sup class="text-red-500">*</sup></label>
                    <input type="text" class="bg-gray-100 border border-gray-200 rounded-md p-2 focus:outline-none focus:ring-gray-900 focus:border-gray-900" id="firstname" wire:model="firstname">
                     @error('firstname') <span class="text-red-500">{{ $message }}</span> @enderror
                </div>
                
                <div class="w-full sm:w-1/2 flex flex-col space-y-1 px-4 mt-4 sm:mt-0">
                    <label for="lastname" class="">{{ trans('messages.last-name') }} <sup class="text-red-500">*</sup></label>
                    <input type="text" class="bg-gray-100 border border-gray-200 rounded-md p-2 focus:outline-none focus:ring-gray-900 focus:border-gray-900" id="lastname" wire:model="lastname">
                     @error('lastname') <span class="text-red-500">{{ $message }}</span> @enderror
                </div>
            </div>
          
            <div class="flex flex-col sm:flex-row sm:flex-wrap -mx-4 mb-4">
                <div class="w-full sm:w-1/2 flex flex-col space-y-1 px-4">
                    <label for="email" class="">{{ trans('messages.e-mail') }} <sup class="text-red-500">*</sup></label>
                    <input type="email" class="bg-gray-100 border border-gray-200 rounded-md p-2 focus:outline-none focus:ring-gray-900 focus:border-gray-900" id="email" wire:model="email" value="{{ old('email') }}">
                    @error('email') <span class="text-red-500">{{ $message }}</span> @enderror
                </div>
                <div class="w-full sm:w-1/2 flex flex-col space-y-1 px-4 mt-4 sm:mt-0">
                    <label for="telephone" class="">{{ trans('messages.telephone') }} <sup class="text-red-500">*</sup></label>
                    <input type="text" class="bg-gray-100 border border-gray-200 rounded-md p-2 focus:outline-none focus:ring-gray-900 focus:border-gray-900" id="telephone" wire:model="telephone" value="{{ old('telephone') }}">
                    @error('telephone') <span class="text-red-500">{{ $message }}</span> @enderror
                </div>
            </div>
          
            <div class="flex flex-col space-y-1 mb-4">
              <label for="brand" class="">{{ trans('messages.brand') }} <sup class="text-red-500">*</sup></label>
              <select id="brand" wire:model="brand" class="bg-gray-100 border border-gray-200 rounded-md p-2 focus:outline-none focus:ring-gray-900 focus:border-gray-900">
                    <option value=''>{{ trans('messages.make-a-choice') }}</option>
                    @foreach (App\Models\Brand::all() as $brand)
                      <option value="{{ $brand->name }}">{{ $brand->name }}</option>
                    @endforeach
              </select>
              @error('brand') <span class="text-red-500">{{ $message }}</span> @enderror
            </div>

            <div class="flex flex-col space-y-1 mb-4">
                <label for="description" class="">{{ trans('messages.description') }} <sup class="text-red-500">*</sup></label>
                <textarea id="description" wire:model="description" rows="8" class="bg-gray-100 border border-gray-200 rounded-md p-2 focus:outline-none focus:ring-gray-900 focus:border-gray-900"></textarea>
                @error(' mb-8') <span class="text-red-500">{{ $message }}</span> @enderror
            </div>

            <div class="flex flex-col space-y-1 mb-4">
                <label for="website" class="">{{ trans('messages.website') }}</label>
                <input type="text" id="website" wire:model="website" placeholder="http://" class="bg-gray-100 border border-gray-200 rounded-md p-2 focus:outline-none focus:ring-gray-900 focus:border-gray-900">
                @error('website') <span class="text-red-500">{{ $message }}</span> @enderror
            </div>

            <div>
                <label for="file" class="">{{ trans('messages.file') }}</label>
                <div class="mt-1 flex items-center">
                  <input type="file" id="file" wire:model="file" class="bg-gray-100 border border-gray-200 rounded-md p-2 focus:outline-none focus:ring-gray-900 focus:border-gray-900">
                </div>
                @error('file') <span class="text-red-500">{{ $message }}</span> @enderror
            </div>

            <div class="flex justify-end">       
                <button type="submit" class="bg-gray-700 hover:bg-gray-900 text-white transition duration-200 rounded-md px-4 py-2 uppercase text-lg">
                    {{ trans('messages.send') }}
                </button>
            </div>
        </form>

    @endif

</div>
