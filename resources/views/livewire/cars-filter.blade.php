<div>

    <div class="bg-gray-50 border border-gray-200 rounded-2xl p-4 mb-8 flex flex-col space-y-2 sm:space-y-0 sm:flex-row sm:justify-between">
        <div class="flex flex-col sm:flex-row sm:items-center space-y-2 sm:space-y-0 sm:space-x-4">

            <input
                type="text"
                wire:model="search"
                placeholder="{{ trans('messages.search-cars',[],session('locale')) }}"
                class="w-full sm:w-56 md:w-64 bg-gray-100 border border-gray-200 rounded-md p-2 focus:outline-none focus:ring-gray-400 transition duration-200 focus:border-gray-400 transition duration-200"
            />

            <select wire:model="fuel" class="w-auto bg-gray-100 placeholder-gray-300 border border-gray-200 rounded-md py-2 pl-2 pr-10 focus:outline-none focus:ring-gray-400 transition duration-200 focus:border-gray-400 transition duration-200">
                <option value="">{{  trans('messages.select-fuel',[],session('locale')) }}</option>
                @foreach (App\Models\Fuel::all() as $fuel)
                    <option value="{{ $fuel->id }}">{{ $fuel->name }}</option>
                @endforeach
            </select>

            <select wire:model="brand" class="w-auto bg-gray-100 placeholder-gray-300 border border-gray-200 rounded-md py-2 pl-2 pr-10 focus:outline-none focus:ring-gray-400 transition duration-200 focus:border-gray-400 transition duration-200">
                <option value="">{{ trans('messages.select-brand',[],session('locale')) }}</option>
                @foreach (App\Models\Brand::all() as $brand)
                <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                @endforeach
            </select>

            <div class="relative flex items-start">
                <div class="flex h-6 items-center">
                    <input id="vat_recoverable" wire:model="vatRecoverable" type="checkbox" class="h-4 w-4 rounded border-gray-300 text-gray-600 focus:ring-gray-600">
                </div>
                <div class="ml-3">
                    <label for="vat_recoverable" class="text-sm font-medium leading-6 text-gray-900">
                        {{ trans('messages.vat-recoverable',[],session('locale')) }}
                    </label>
                </div>
            </div>




        </div>

        <button type="button" wire:click="resetFilters" class="bg-gray-900 hover:bg-gray-700 text-white transition duration-200 rounded-md px-4 py-2 uppercase text-lg">
            {{ trans('messages.reset-filters',[],session('locale')) }}
        </button>
    </div>

    <div class="max-w-7xl mx-auto grid gap-6 md:gap-8 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">

        @foreach($cars as $car)
            <a href="{{ localization()->getUrlFromRouteName(localization()->getCurrentLocale(), 'routes.our-vehicles/car', $attributes = []) }}/{{ $car->slug }}">
                <div class="car {{$car->brand->slug}}" data-price="{{ str_replace('.','',$car->price) }}" data-date="{{ $car->created_at->format('Ymd') }}">
                    @if ($image = $car->getMedia('photos')->first())
                    <img src="{{ $image->getUrl('thumb') }}" class="rounded-2xl" />
                    @else
                    <img src="{{ asset('img/placeholder.png') }}" class="rounded-2xl" />
                    @endif
                    <div style="min-height: 90px;">
                        <h3>{{ $car->title }}</h3>
                        <p class="car-price">&euro; {{ $car->price }}</p>
                    </div>
                </div>
            </a>
        @endforeach

    </div>
</div>
