<div class="pr-0 md:pr-8">
    
    @if (count($errors) > 0)
        <div class="bg-red-100 border-red-200 p-2 rounded text-red-800 mb-8">
            {{ trans('messages.all-fields') }}
        </div>
    @endif
    
    @if (Session::has('status') )
        <div class="bg-green-100 border-green-200 p-2 rounded text-green-800 mb-8">
            {{ Session::get('status') }}
        </div>
    @else

        <form wire:submit.prevent="submit">
      
            <div class="flex flex-col sm:flex-row sm:flex-wrap -mx-4 mb-4">
                <div class="w-full sm:w-1/2 flex flex-col space-y-1 px-4">
                    <label for="firstname" class="">{{ trans('messages.first-name') }} <sup class="text-red-500">*</sup></label>
                    <input type="text" class="bg-gray-100 border border-gray-200 rounded-md p-2" id="firstname" wire:model="firstname">
                     @error('firstname') <span class="text-red-500">{{ $message }}</span> @enderror
                </div>
                
                <div class="w-full sm:w-1/2 flex flex-col space-y-1 px-4 mt-4 sm:mt-0">
                    <label for="lastname" class="">{{ trans('messages.last-name') }} <sup class="text-red-500">*</sup></label>
                    <input type="text" class="bg-gray-100 border border-gray-200 rounded-md p-2" id="lastname" wire:model="lastname">
                     @error('lastname') <span class="text-red-500">{{ $message }}</span> @enderror
                </div>
            </div>
          
            <div class="flex flex-col sm:flex-row sm:flex-wrap -mx-4 mb-4">
                <div class="w-full sm:w-1/2 flex flex-col space-y-1 px-4">
                    <label for="email" class="">{{ trans('messages.e-mail') }} <sup class="text-red-500">*</sup></label>
                    <input type="email" class="bg-gray-100 border border-gray-200 rounded-md p-2" id="email" wire:model="email" value="{{ old('email') }}">
                    @error('email') <span class="text-red-500">{{ $message }}</span> @enderror
                </div>
                <div class="w-full sm:w-1/2 flex flex-col space-y-1 px-4 mt-4 sm:mt-0">
                    <label for="telephone" class="">{{ trans('messages.telephone') }} <sup class="text-red-500">*</sup></label>
                    <input type="text" class="bg-gray-100 border border-gray-200 rounded-md p-2" id="telephone" wire:model="telephone" value="{{ old('telephone') }}">
                    @error('telephone') <span class="text-red-500">{{ $message }}</span> @enderror
                </div>
            </div>
          
            <div class="flex flex-col space-y-1 mb-4">
                <label for="subject" class="">{{ trans('messages.subject') }} <sup class="text-red-500">*</sup></label>
                <input type="text" class="bg-gray-100 border border-gray-200 rounded-md p-2" id="subject" wire:model="subject">
                @error('subject') <span class="text-red-500">{{ $message }}</span> @enderror
            </div>

            <div class="flex flex-col space-y-1 mb-4">
                <label for="message" class="">{{ trans('messages.message') }} <sup class="text-red-500">*</sup></label>
                <textarea class="bg-gray-100 border border-gray-200 rounded-md p-2" id="message" wire:model="message" rows="8"></textarea>
                @error(' mb-8') <span class="text-red-500">{{ $message }}</span> @enderror
            </div>

            <div class="flex justify-end">       
                <button type="submit" class="bg-gray-700 hover:bg-gray-900 text-white transition duration-200 rounded-md px-4 py-2 uppercase text-lg">
                    {{ trans('messages.send') }}
                </button>
            </div>
        </form>

    @endif

</div>
