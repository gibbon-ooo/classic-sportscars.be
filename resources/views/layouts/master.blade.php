<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	@foreach (localization()->getSupportedLocalesKeys() as $locale)
    @if (Route::current())
      <link rel="alternate" hreflang="{{$locale}}" href="{{ localization()->localizeURL( url()->current(), $locale ) }}">
    @endif
  @endforeach



  <title>Classic and Sportscars | @yield ('title')</title>
	<meta name="robots" content="index, follow">

	@if (localization()->getCurrentLocale() == 'fr')
	<meta name="keywords" content="achat, voiture, occasion, vente, Belgique, voitures de sport et de prestige">
	@elseif (localization()->getCurrentLocale() == 'nl')
	<meta name="keywords" content="aankoop, wagen, auto, tweedehands, aankoop, België, sportwagens, prestigewagens">
	@elseif (localization()->getCurrentLocale() == 'en')
	<meta name="keywords" content="buying, vehicles, cars, second hand, selling, Belgium, sports cars, prestige cars">
	@endif

	@yield ('seo')

  <link rel="stylesheet" href="{{ asset('css/app.css') }}" />

  @yield ('pagestyles')


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-84288468-1', 'auto');
	  ga('send', 'pageview');

	</script>
  </head>

  <body>
    <div id="wrap">

		<header role="banner">

			<ul id="language-picker">
				<li><a rel="alternate" hreflang="fr" href="{{ localization()->getLocalizedURL('fr') }}" <?php if (localization()->getCurrentLocale() == "fr") echo 'class="active"'; ?>>FR</a></li>
				<li><a rel="alternate" hreflang="nl" href="{{ localization()->getLocalizedURL('nl') }}" <?php if (localization()->getCurrentLocale() == "nl") echo 'class="active"'; ?>>NL</a></li>
				<li><a rel="alternate" hreflang="en" href="{{ localization()->getLocalizedURL('en') }}" <?php if (localization()->getCurrentLocale() == "en") echo 'class="active"'; ?>>EN</a></li>
			</ul>

			<ul id="social-media">
				<li><a href="https://www.instagram.com/wwwclassicsportscarsbe/" title="Instagram"><img src="{{ asset('img/icons/instagram.svg') }}" width="30" height="30" /></a></li>
				<li><a href="https://www.facebook.com/classicandsportscars.be" title="Facebook"><img src="{{ asset('img/icons/facebook.svg') }}" width="25" height="25" /></a></li>
			</ul>



			<img id="logo-main" src="{{ asset('img/logo.svg') }}" class="img-responsive" alt="Classic and Sportscars">

			<nav id="navbar-primary" class="navbar navbar-inverse" role="navigation">
			  <div class="container-fluid">
			    <!-- Brand and toggle get grouped for better mobile display -->
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-primary-collapse">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			    </div>
			    <div class="collapse navbar-collapse" id="navbar-primary-collapse">
			      <ul class="nav navbar-nav">
			        <li id="first-menu-item"><a href="{{ localization()->getUrlFromRouteName(localization()->getCurrentLocale(), 'routes.home') }}" class="active">{{ trans('messages.home') }}</a></li>
			        <li><a href="{{ localization()->getUrlFromRouteName(localization()->getCurrentLocale(), 'routes.our-vehicles') }}">{{ trans('messages.our-vehicles') }}</a></li>
			        <li><a href="{{ localization()->getUrlFromRouteName(localization()->getCurrentLocale(), 'routes.buying-and-selling') }}">{{ trans('messages.buying-and-selling') }}</a></li>
			        <li><a href="{{ localization()->getUrlFromRouteName(localization()->getCurrentLocale(), 'routes.gallery') }}">{{ trans('messages.gallery') }}</a></li>
			        <li><a href="{{ localization()->getUrlFromRouteName(localization()->getCurrentLocale(), 'routes.who-we-are') }}">{{ trans('messages.who-we-are') }}</a></li>
			        <li><a href="{{ localization()->getUrlFromRouteName(localization()->getCurrentLocale(), 'routes.contact') }}"><i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp;&nbsp;{{ trans('messages.contact') }}</a></li>
			      </ul>
			    </div><!-- /.navbar-collapse -->

			  </div><!-- /.container-fluid -->
			</nav>

		</header><!-- header role="banner" -->

		@yield('banner')

		<div class="container-fluid page">
			@yield('content')
		</div>
	</div>

	<footer>
		<div class="container">
			<p class="text-center"><a href="+3223745015">+32 (0)2-374 50 15</a>  •  Zoniënwoudlaan 155 - 1640 Sint Genesius Rode - België</p>
			<p class="text-center">COPYRIGHT &copy; {{ date('Y') }} • ALL RIGHTS RESERVED • WEBDESIGN BY <a href="https://www.gibbon.ooo">Gibbon</a></p>
		</div>
    </footer>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    @yield('pagescripts')
  </body>
</html>
