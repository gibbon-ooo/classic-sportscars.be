<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Classic and Sportscars</title>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;700&family=PT+Sans:wght@400;700&display=swap" rel="stylesheet"> 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Glide.js/3.2.0/css/glide.core.min.css" integrity="sha512-YQlbvfX5C6Ym6fTUSZ9GZpyB3F92hmQAZTO5YjciedwAaGRI9ccNs4iw2QTCJiSPheUQZomZKHQtuwbHkA9lgw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        @livewireStyles
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body class="antialiased font-pt-sans">

        <nav>
            
            <div class="relative px-4 py-4 md:py-8 bg-gray-800">
                <x-locales />

                <a href="/">
                    <img src="{{ asset('img/logo.svg') }}" class="w-44 md:w-64 mx-auto">
                </a>

                <x-social />
            </div>

            <div class="relative px-4 py-4 bg-gray-700">

                <div x-data="{open: false}" x-cloak>
                    <div class="flex justify-end block lg:hidden">
                        <button x-on:click="open = !open" class="text-white p-2" >
                            <svg class="w-6 h-6 fill-current" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16"></path></svg>
                        </button>
                    </div>

                    <div x-show.transition="open" class="fixed inset-0 block bg-gray-700 h-full w-full flex justify-center items-center" style="z-index: 9999;">
                        
                        <button x-on:click="open = false" class="absolute top-0 right-0 mt-4 mr-4 text-white p-2">
                            <svg class="w-10 h-10 fill-current" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path></svg>
                        </button>

                        <ul x-show.transition="open" class="text-white flex flex-col items-center space-y-6 font-medium font-open-sans text-2xl">
                            <li>
                                <a href="{{ localization()->getUrlFromRouteName(localization()->getCurrentLocale(), 'routes.home') }}">
                                    {{ trans('messages.home') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ localization()->getUrlFromRouteName(localization()->getCurrentLocale(), 'routes.our-vehicles') }}">
                                    {{ trans('messages.our-vehicles') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ localization()->getUrlFromRouteName(localization()->getCurrentLocale(), 'routes.buying-and-selling') }}">
                                    {{ trans('messages.buying-and-selling') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ localization()->getUrlFromRouteName(localization()->getCurrentLocale(), 'routes.gallery') }}">
                                    {{ trans('messages.gallery') }}
                                </a>
                            </li>

                            <li>
                                <a href="{{ localization()->getUrlFromRouteName(localization()->getCurrentLocale(), 'routes.who-we-are') }}">
                                    {{ trans('messages.who-we-are') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ localization()->getUrlFromRouteName(localization()->getCurrentLocale(), 'routes.contact') }}">
                                    {{ trans('messages.contact') }}
                                </a>
                            </li>
                        </ul> 
                    </div>
                </div>

                <ul class="hidden lg:flex text-white justify-center space-x-6 font-medium font-open-sans text-lg">
                    <li>
                        <a href="{{ localization()->getUrlFromRouteName(localization()->getCurrentLocale(), 'routes.home') }}" class="active">
                            {{ trans('messages.home') }}
                        </a>
                    </li>
                    <li>
                        <a href="{{ localization()->getUrlFromRouteName(localization()->getCurrentLocale(), 'routes.our-vehicles') }}">
                            {{ trans('messages.our-vehicles') }}
                        </a>
                    </li>
                    <li>
                        <a href="{{ localization()->getUrlFromRouteName(localization()->getCurrentLocale(), 'routes.buying-and-selling') }}">
                            {{ trans('messages.buying-and-selling') }}
                        </a>
                    </li>
                    <li>
                        <a href="{{ localization()->getUrlFromRouteName(localization()->getCurrentLocale(), 'routes.gallery') }}">
                            {{ trans('messages.gallery') }}
                        </a>
                    </li>

                    <li>
                        <a href="{{ localization()->getUrlFromRouteName(localization()->getCurrentLocale(), 'routes.who-we-are') }}">
                            {{ trans('messages.who-we-are') }}
                        </a>
                    </li>
                    <li>
                        <a href="{{ localization()->getUrlFromRouteName(localization()->getCurrentLocale(), 'routes.contact') }}">
                            {{ trans('messages.contact') }}
                        </a>
                    </li>
                </ul>                
            </div>
            

        </nav>

        @yield('content')

        <footer class="text-xs font-medium text-gray-500 text-center px-4 py-8 space-y-1 mt-8 border-t border-gray-100">


            <div class="flex justify-center items-center space-x-4 mb-8">

                @if (localization()->getCurrentLocale() == 'nl')
                    <a href="http://www.erkendverkoper.be" target="_blank">
                        <img src="{{ asset('/img/erkend-verkoper.png') }}" class="w-16 h-16" />
                    </a>
                @elseif (localization()->getCurrentLocale() == 'fr')
                    <a href="http://www.vendeuragree.be" target="_blank">
                        <img src="{{ asset('/img/vendeur-agree.png') }}" class="w-16 h-16" />
                    </a>
                @elseif (localization()->getCurrentLocale() == 'en')
                    <a href="http://www.erkendverkoper.be" target="_blank">
                        <img src="{{ asset('/img/erkend-verkoper.png') }}" class="w-16 h-16" />
                    </a>
                @endif

                <a href="http://www.traxio.be/fr" target="_blank" />
                    <img src="{{ asset('img/carpass-federauto.png') }}" width="300" />
                </a>
            </div>

            <p><a href="tel:+3223745015" class="hover:underline">+32 (0)2-374 50 15</a></p>
            <p><a href="https://www.google.com/maps/place/Classic+%26+Sportscars/@50.7453154,4.3507723,17z/data=!4m13!1m7!3m6!1s0x47c3cf9843717ce5:0xd9adf241497eb0cb!2sZoni%C3%ABnwoudlaan+155,+1640+Sint-Genesius-Rode!3b1!8m2!3d50.7453154!4d4.3529663!3m4!1s0x47c3c4e1865d4583:0x8df2902be63f0988!8m2!3d50.7452887!4d4.352894" class="hover:underline">Zoniënwoudlaan 155 - 1640 Sint Genesius Rode - België</a></p>
            <p>COPYRIGHT &copy; {{ date('Y') }} • All rights reserved • Webdesign by <a href="https://gibbon.ooo" class="hover:underline">Gibbon</a></p>
        </footer>


        <script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>    
        @livewireScripts
        <script src="https://unpkg.com/@popperjs/core@2"></script>
        <script src="https://unpkg.com/tippy.js@6"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Glide.js/3.2.0/glide.min.js" integrity="sha512-IkLiryZhI6G4pnA3bBZzYCT9Ewk87U4DGEOz+TnRD3MrKqaUitt+ssHgn2X/sxoM7FxCP/ROUp6wcxjH/GcI5Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="{{ asset('js/app.js') }}"></script>

        @yield('pagescripts')
    </body>
</html>
