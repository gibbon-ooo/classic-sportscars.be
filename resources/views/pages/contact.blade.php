@extends('layouts.app')

@section('description')
	@if (localization()->getCurrentLocale() == 'fr')
	@elseif (localization()->getCurrentLocale() == 'nl')
	@elseif (localization()->getCurrentLocale() == 'en')
	@endif
@endsection

@section('title', trans("messages.contact-title"))

@section('content')
	
	<header>
		<a href="https://www.google.be/maps/place/Classic+%26+Sportscars/@50.7452879,4.3506701,17z/data=!3m1!4b1!4m5!3m4!1s0x47c3c4e1865d4583:0x8df2902be63f0988!8m2!3d50.7452845!4d4.3528641">
			<img src="{{ asset('img/map_header.jpg') }}" class="w-full"/>
		</a>
	</header>
	
    <section class="px-4 md:px-8 py-12 md:py-20 max-w-7xl mx-auto">
		
		<div class="flex flex-col md:flex-row md:flex-wrap">
			
			@fr
				<div class="w-full md:w-3/4 mb-12 md:mb-0">					
					<x-h1 class="text-xl font-bold font-open-sans mb-4 uppercase">Itinéraire</x-h1>
					<strong>Venant de France</strong>
					<ul class="list-disc pl-8 mb-4">
						<li>E19 Mons Tournai en direction Bruxelles</li>
						<li>Sortie Alsemberg – Huizingen à droite en direction de Rhode St Genèse</li>
						<li>Suivez la chaussée d'Alsemberg jusqu'au centre d'Alsemberg</li>
						<li>Continuez sur l'Avenue de la Forêt de Soignes</li>
						<li>Vous nous trouvez sur la gauche au numéro 155</li>
					</ul>

					<strong>Venant d'Anvers, Malines où Anvers</strong>
					<ul class="list-disc pl-8 mb-4">
						<li>E19 venant d’Anvers en direction de Bruxelles</li>
						<li>R0 en direction de Charleroi</li>
						<li>Sortie Watermael-Boitsfort – deuxième à droite en direction de Rhode St Genèse</li>
						<li>Suivez l'avenue Dubois dans la Fôret de Soignes</li>
						<li>Quand vous sortez du bois tournez vers la gauche en direction de Waterloo</li>
						<li>Au carrefour Chaussée de Waterloo avec l'Avenue de la Forêt de Soignes - tournez à droite</li>
						<li>Vous nous trouvez sur l'Avenue de la Forêt de Soignes au numéro 155</li>
					</ul>	
					
					<strong>Venant de la Flandre Occidentale & Orientale</strong>
					<ul class="list-disc pl-8 mb-4">
						<li>E40 en direction de Bruxelles</li>
						<li>R0 en direction de Charleroi</li>
						<li>Sortie Alsemberg – Huizingen à gauche en direction de Rhode St Genèse</li>
						<li>Suivez la chaussée d'Alsemberg jusqu'au centre d'Alsemberg</li>
						<li>Continuez sur l'Avenue de la Forêt de Soignes</li>
						<li>Vous nous trouvez sur la gauche au numéro 155</li>
					</ul>
					
					<strong>Venant des Pays Bas - Liège - Louvain</strong>
					<ul class="list-disc pl-8 mb-4">
						<li>E40 où E314 en direction de Bruxelles</li>
						<li>R0 en direction de Charleroi</li>
						<li>Sortie Watermael-Boitsfort – deuxième à droite en direction de Rhode St Genèse</li>
						<li>Suivez l'avenue Dubois dans la Fôret de Soignes</li>
						<li>Quand vous sortez du bois tournez vers la gauche en direction de Waterloo</li>
						<li>Au carrefour Chaussée de Waterloo avec l'Avenue de la Forêt de Soignes - tournez à droite</li>
						<li>Vous nous trouvez sur l'Avenue de la Forêt de Soignes au numéro 155</li>
					</ul>

					<strong>En train</strong>
					<ul class="list-disc pl-8 mb-4">
						<li>Train IC, IR & Thalys gare Bruxelles MIDI</li>
						<li>Gare Rhode St Genèse pour les trains locaux</li>
						<li>Depuis la gare, parcourez 950m sur l'avenue de la Forêt de Soignes jusqu'au numéro 155</li>
						<li>Vous pouvez calculer votre itinéraire via le <a href="https://www.stib-mivb.be/reisweg-itineraire.html?l=fr" target="_blank"/>site web de la STIB</a></li>
					</ul>
					<br>
					<br>


					<h1 class="text-xl font-bold font-open-sans mb-4 uppercase">CONTACT</h1>
					
					<livewire:contact-form />
					
				</div>
				
				<div class="w-full md:w-1/4 mb-12 md:mb-0 bg-gray-100 border border-gray-200 p-4 rounded-xl">					
					<p><strong>Classic and Sportscars SRL</strong></p>
					<p>Bureau & Showroom</p>
					<p>Avenue de la Forêt de Soignes 155</p>
					<p>1640 Rhode St Genèse</p>
					<p>Belgique</p>
					<br />
					<p>Ouverture du Lundi au Vendredi</p>
					<p>9h00 à 18h00 (sans interruption)</p>
					<p>Samedi: 10h00 à 13h00</p>
					<br>
					<p>Tél: <a href="tel:+3223745015" class="text-blue-500 hover:text-blue-700 hover:underline">+32 2 (0)374.50.15</a></p>
					<p>Fax: +32 2 (0) 374.18.81</p>
					<p>Mail: <a href="mailto:info@classic-sportscars.be" class="text-blue-500 hover:text-blue-700 hover:underline">info@classic-sportscars.be</a></p> 
					<br>
					<p><a href="https://www.google.com/maps/place/Classic+%26+Sportscars/@50.7448847,4.3483666,17z/data=!3m1!4b1!4m5!3m4!1s0x47c3c4e1865d4583:0x8df2902be63f0988!8m2!3d50.7448847!4d4.3505553" target="_blank" class="text-blue-500 hover:text-blue-700 hover:underline">Google maps</a></p>
					<br>
					<p><strong>Service Vente & Après-Vente</strong></p>
					<p>James-Oliver Moyses</p>
					<p>Tél: <a href="tel:+3223745015" class="text-blue-500 hover:text-blue-700 hover:underline">+32 2 (0)374.50.15</a></p>
					<p>Fax: +32 2 (0) 374.18.81</p>
					<br>
					<p><strong>Service Achat / Reprise</strong></p>
					<p>Julian Zenner</p>
					<p>Tél: <a href="tel:+3223745015" class="text-blue-500 hover:text-blue-700 hover:underline">+32 2 (0)374.50.15</a></p>
					<p>Mail: <a href="mailto:jz@classic-sportscars.be" class="text-blue-500 hover:text-blue-700 hover:underline">jz@classic-sportscars.be</a></p> 
					<br>
					<p><strong>Siège social:</strong></p>
					<p>Classic and Sportscars SRL</p>
					<p>Rue de la longue haie 66</p>
					<p>B-1000 Bruxelles (Belgique)</p>
					<br>			
					<p><strong>Coordonnées bancaire (RIB)</strong></p>
					<ul>
						<li>
							<p>Nom de la Banque: KBC S.A</p>
							<p>Adresse : Av du Port 2, 1080 Brussels (Belgium)</p>
							<p>N° de compte bancaire: IBAN BE78 7310 0769 3586</p>
							<p>BIC KREDBEBB</p>
							<img src="{{ asset('img/KBC_Bank.png') }}" width="70" class="mt-2"/>
							<br>
							<br>
						</li>
	
						<li>
							<p>Nom de la Banque: BNP Paribas Fortis</p>
							<p>N° de compte bancaire: IBAN BE22 1431 0682 0747</p>
							<p>BIC GEBABEBB</p>
							<img src="{{ asset('img/BNP_Paribas_Fortis.png') }}" width="80" class="mt-2"/>
							<br>
						</li>
					</ul>

				</div>

			@elsenl
			
				<div class="w-full md:w-3/4 mb-12 md:mb-0">					
					<x-h1 class="text-xl font-bold font-open-sans mb-4 uppercase">Routebeschrijving</x-h1>
					<strong>Vanuit de richting van Nederland, Mechelen of Antwerpen</strong>
					<ul class="list-disc pl-8 mb-4">
						<li>E19 van Antwerpen richting Brussel</li>
						<li>R0 richting Charleroi</li>
						<li>Afrit Watermael-Bosvoorde – 2de rechts richting Sint Genesius Rode</li>
						<li>Volg richting van Sint Genesius Rode – Duboislaan</li>
						<li>Volg eens het bos uit de richting van Waterloo – Waterloosesteenweg</li>
						<li>Aan het kruispunt met de Zoniënwoudlaan volg richting Sint Genesius Rode</li>
						<li>U vind ons op de Zoniënwoudlaan nummer 155</li>
					</ul>	
					
					<strong>​​Vanuit de richting van Oost & West Vlaanderen</strong>
					<ul class="list-disc pl-8 mb-4">
						<li>E40 richting Brussel</li>
						<li>R0 richting Charleroi</li>
						<li>Afrit 20 Alsemberg – Huizingen links richting Sint Genesius Rode</li>
						<li>Volg richting van de Alsembergsesteenweg</li>
						<li>Nadat de Alsembergsesteenweg overgaat in de Zoniënwoudlaan komt u na 1km aan uw linkerzijde aan het nummer 155</li>
					</ul>	
					
					<strong>Vanuit de richting Nederland – Limburg – Leuven</strong>
					<ul class="list-disc pl-8 mb-4">
						<li>E40 of E314 richting Brussel</li>
						<li>R0 richting Charleroi</li>
						<li>Afrit Watermael-Bosvoorde – 2de rechts richting Sint Genesius Rode</li>
						<li>Volg richting van Sint Genesius Rode – Duboislaan</li>
						<li>Volg eens het bos uit de richting van Waterloo – Waterloosesteenweg</li>
						<li>Aan het kruispunt met de Zoniënwoudlaan volg richting Sint Genesius Rode</li>
						<li>U vind ons op de Zoniënwoudlaan nummer 155</li>
					</ul>

					<strong>​​Vanuit Frankrijk</strong>
					<ul class="list-disc pl-8 mb-4">
						<li>E19 Doornik-Bergen richting Brussel</li>
						<li>Afrit 20 Alsemberg – Huizingen rechts richting Sint Genesius Rode</li>
						<li>Volg richting van de Alsembergsesteenweg</li>
						<li>Nadat de Alsembergsesteenweg overgaat in de Zoniënwoudlaan komt u na 1km aan uw linkerzijde aan het nummer 155</li>
					</ul>

					<strong>Met de trein</strong>
					<ul class="list-disc pl-8 mb-4">
						<li>IC, IR en Thalys Station Brussel ZUID</li>
						<li>Station Sint Genesius Rode voor lokale treinen</li>
						<li>Vanaf het Station wandelt u 950m via de Zoniënwoudlaan tot aan het nummer 155</li>
						<li>U kan uw route berekenen via de handige <a href="https://www.stib-mivb.be/reisweg-itineraire.html?l=fr" target="_blank"/>website van de MIVB</a></li>
					</ul>
					
					<br>
					<br>
					
					<h1 class="text-xl font-bold font-open-sans mb-4 uppercase">CONTACT</h1>
					<livewire:contact-form />

				</div>

				<div class="w-full md:w-1/4 mb-12 md:mb-0 bg-gray-100 border border-gray-200 p-4 rounded-xl">					
					<p><strong>Classic & Sportscars BV</strong></p>
					<p>Kantoren & Showroom</p>
					<p>Zoniënwoudlaan 155</p>
					<p>1640 Sint Genesius Rode</p>
					<p>België</p>
					<br />
					<p>Open van maandag tot vrijdag</p>
					<p>9u00 à 18u00 (doorlopend)</p>
					<p>Zaterdag: 10u00 à 13u00</p>
					<br>
					<p>Tel: <a href="tel:+3223745015" class="text-blue-500 hover:text-blue-700 hover:underline">+32 2 (0)374.50.15</a></p>
					<p>Fax: +32 2 (0) 374.18.81</p>
					<p>E-mail: <a href="mailto:info@classic-sportscars.be" class="text-blue-500 hover:text-blue-700 hover:underline">info@classic-sportscars.be</a></p> 
					<br>
					<p><a href="https://www.google.com/maps/place/Classic+%26+Sportscars/@50.7448847,4.3483666,17z/data=!3m1!4b1!4m5!3m4!1s0x47c3c4e1865d4583:0x8df2902be63f0988!8m2!3d50.7448847!4d4.3505553" target="_blank" class="text-blue-500 hover:text-blue-700 hover:underline">Google maps</a></p>
					<br>
					<p><strong>Dienst verkoop & naverkoop</strong></p>
					<p>James-Oliver Moyses</p>
					<p>Tel: <a href="tel:+3223745015" class="text-blue-500 hover:text-blue-700 hover:underline">+32 2 (0)374.50.15</a></p>
					<p>Fax: +32 2 (0) 374.18.81</p>
					<br>
					<p><strong>Dienst aankoop & overname</strong></p>
					<p>Julian Zenner</p>
					<p>Tel: <a href="tel:+3223745015" class="text-blue-500 hover:text-blue-700 hover:underline">+32 2 (0)374.50.15</a></p>
					<p>Mail: <a href="mailto:jz@classic-sportscars.be" class="text-blue-500 hover:text-blue-700 hover:underline">jz@classic-sportscars.be</a></p> 
					<br>
					<br>
					<p><strong>Maatschappelijke zetel:</strong></p>
					<p>Classic and Sportscars BV</p>
					<p>Langehaagstraat 66</p>
					<p>B-1000 Brussel (België)</p>
					<br>
					<p><strong>Bankgegevens</strong></p>
					<ul>
						<li>
							<p>Naam bank: KBC S.A</p>
							<p>Adres : Av du Port 2, 1080 Brussels (Belgium)</p>
							<p>Rekeningnummer: IBAN BE78 7310 0769 3586</p>
							<p>BIC KREDBEBB</p>
							<img src="{{ asset('img/KBC_Bank.png') }}" width="70" class="mt-2"/>
							<br>
							<br>
						</li>
	
						<li>
							<p>Naam bank: BNP Paribas Fortis</p>
							<p>Rekeningnummer: IBAN BE22 1431 0682 0747</p>
							<p>BIC GEBABEBB</p>
							<img src="{{ asset('img/BNP_Paribas_Fortis.png') }}" width="80" class="mt-2" />
							<br>
						</li>
					</ul>

				</div>

			@elseen
				
				<div class="w-full md:w-3/4 mb-12 md:mb-0">					
					<x-h1 class="text-xl font-bold font-open-sans mb-4 uppercase">Travel directions</x-h1>
					<strong>From the Netherlands, Mechelen or Antwerp</strong>
					<ul class="list-disc pl-8 mb-4">
						<li>E19 from Antwerp direction Brussels</li>
						<li>R0 direction Charleroi</li>
						<li>Exit Watermael-Bosvoorde – keep 2nd right direction Sint Genesius Rode</li>
						<li>Follow the Duboislaan - direction Sint Genesius Rode</li>
						<li>Once through the forest follow the direction of Waterloo - Waterloosesteenweg</li>
						<li>At the intersection with the Zoniënwoudlaan, follow the direction of Sint Genesius Rode</li>
						<li>You can find us on the Zoniënwoudlaan, number 155</li>
					</ul>	
					
					<strong>From East and West Flanders</strong>
					<ul class="list-disc pl-8 mb-4">
						<li>E40 direction Brussels</li>
						<li>R0 direction Charleroi</li>
						<li>Exit Alsemberg – Huizingen left direction Sint Genesius Rode</li>
						<li>Follow the Alsembergsesteenweg in the direction to Sint Genesius Rode</li>
						<li>Once that the Alsembergsesteenweg turns into Zoniënwoudlaan, after 1 km you will find us at number 155</li>
					</ul>	
					
					<strong>From the Netherlands – Limburg – Leuven</strong>
					<ul class="list-disc pl-8 mb-4">
						<li>E40 or E314 directions Brussel</li>
						<li>R0 directions Charleroi</li>
						<li>Follow the Duboislaan - direction Sint Genesius Rode</li>
						<li>Once through the forest follow the direction of Waterloo - Waterloosesteenweg</li>
						<li>At the intersection with the Zoniënwoudlaan, follow the direction of Sint Genesius Rode</li>
						<li>You can find us on the Zoniënwoudlaan, number 155</li>
					</ul>

					<strong>From France</strong>
					<ul class="list-disc pl-8 mb-4">
						<li>E19 Doornik-Bergen direction Brussels</li>
						<li>Exit Alsemberg – Huizingen right direction Sint Genesius Rode</li>
						<li>Follow the Alsembergsesteenweg in the direction to Sint Genesius Rode</li>
						<li>Once that the Alsembergsesteenweg turns into Zoniënwoudlaan, after 1 km you will find us at number 155</li>
					</ul>

					<strong>By train</strong>
					<ul class="list-disc pl-8 mb-4">
						<li>IC, IR and Thalys trains Railway station Brussels South</li>
						<li>Railway station Sint Genesius Rode for local trains</li>
						<li>From the station, walk 950m along the Zoniënwoudlaan to number 155</li>
						<li>You can calculate your on the <a href="http://www.stib-mivb.be">MIVB website</a></li>
					</ul>
					
					<br>
					<br>

					<h1 class="text-xl font-bold font-open-sans mb-4 uppercase">CONTACT</h1>
					<livewire:contact-form />

				</div>

				<div class="w-full md:w-1/4 mb-12 md:mb-0 bg-gray-100 border border-gray-200 p-4 rounded-xl">					
					<p><strong>Classic & Sportscars BV</strong></p>
					<p>Offices & Showroom</p>
					<p>Zoniënwoudlaan 155</p>
					<p>1640 Sint Genesius Rode</p>
					<p>Belgium</p>
					<br />
					<p>Open from monday to friday</p>
					<p>9h00 to 18h00</p>
					<p>Saturday: 10h00 à 13h00</p>
					<br>
					<p>Tel: <a href="tel:+3223745015" class="text-blue-500 hover:text-blue-700 hover:underline">+32 2 (0)374.50.15</a></p>
					<p>Fax: +32 2 (0) 374.18.81</p>
					<p>E-mail: <a href="mailto:info@classic-sportscars.be" class="text-blue-500 hover:text-blue-700 hover:underline">info@classic-sportscars.be</a></p> 
					<br>
					<p><a href="https://www.google.com/maps/place/Classic+%26+Sportscars/@50.7448847,4.3483666,17z/data=!3m1!4b1!4m5!3m4!1s0x47c3c4e1865d4583:0x8df2902be63f0988!8m2!3d50.7448847!4d4.3505553" target="_blank" class="text-blue-500 hover:text-blue-700 hover:underline">Google maps</a></p>
					<br>
					<p><strong>Sales & after-sales service</strong></p>
					<p>James-Oliver Moyses</p>
					<p>Tel: <a href="tel:+3223745015">+32 2 (0)374.50.15</a></p>
					<p>Fax: +32 2 (0) 374.18.81</p>
					<br>
					<p><strong>Buying service</strong></p>
					<p>Julian Zenner</p>
					<p>Tel: <a href="tel:+3223745015" class="text-blue-500 hover:text-blue-700 hover:underline">+32 2 (0)374.50.15</a></p>
					<p>Mail: <a href="mailto:jz@classic-sportscars.be" class="text-blue-500 hover:text-blue-700 hover:underline">jz@classic-sportscars.be</a></p> 
					<br>
					<br>
					<p><strong>Registered Office:</strong></p>
					<p>Classic and Sportscars SPRL</p>
					<p>Rue de la longue haie 66</p>
					<p>B-1000 Bruxelles (Belgique)</p>
					<br>					
					<p><strong>Bank details</strong></p>
					<ul>
						<li>
							<p>Name bank: KBC S.A</p>
							<p>Address : Av du Port 2, 1080 Brussels (Belgium)</p>
							<p>Bank account N°: IBAN BE78 7310 0769 3586</p>
							<p>BIC KREDBEBB</p>
							<img src="{{ asset('img/KBC_Bank.png') }}" width="70" class="mt-2"/>
							<br>
							<br>
						</li>
	
						<li>
							<p>Name bank: BNP Paribas Fortis</p>
							<p>Bank account N°: IBAN BE22 1431 0682 0747</p>
							<p>BIC GEBABEBB</p>
							<img src="{{ asset('img/BNP_Paribas_Fortis.png') }}" width="80" class="mt-2" />
							<br>
						</li>
					</ul>
					
				</div>	

			@endfr
		
		</div>

		
    </div>
       
@endsection