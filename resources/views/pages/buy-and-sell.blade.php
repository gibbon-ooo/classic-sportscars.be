@extends('layouts.app')

@section('description')
	@if (localization()->getCurrentLocale() == 'fr')
	@elseif (localization()->getCurrentLocale() == 'nl')
	@elseif (localization()->getCurrentLocale() == 'en')
	@endif
@endsection

@section('title', trans("messages.buying-and-selling"))

@section('seo')
	@if (localization()->getCurrentLocale() == 'fr')
		<meta name="description" content="Classic and Sportscars a été fondé par deux entrepreneurs associé, James-Oliver Moyses s’occupant intégralement des ventes et du service après-vente  et le second après avoir fait des études dans une haute école de commerce à décider de lancer dans le secteur automobile Julian Zenner s’occupe intégralement du secteur achat.">
	@elseif (localization()->getCurrentLocale() == 'nl')
		<meta name="description" content="De doelstelling die we betrachten is het aanleveren van zorgvuldig geselecteerde wagens vanuit een milieu van wagenverzamelaars en liefhebbers en gepassioneerde eigenaars. Wagens die in uitmuntende staat verkeren en waarvan de historiek bekend is tot in detail.">
	@elseif (localization()->getCurrentLocale() == 'en')
		<meta name="description" content="Our goal is to sell high quality vehicles that have been fully serviced and have a continuous history at the best prices on the market. We avoid any cars that have been crashed in the past to guarantee the quality of our stock.">
	@endif
@endsection

@section('content')

    <section class="px-4 md:px-8 py-12 md:py-20 max-w-7xl mx-auto">
		
		<div>
			@if (localization()->getCurrentLocale() == 'fr')
				
				<x-h1>ACHAT ET VENTE</x-h1>						
				
				<x-h2>Rachat de votre voiture</x-h2>
				<p>Nous sommes principalement à la recherche de véhicules allemands de sports, de prestiges, 4X4 , S.U.V et même ancêtres.</p>
				<p>C'est dans ce cadre que nous vous proposons l'éventuelle reprise de votre ancien véhicule, que vous nous achetiez une voiture ou non (achat "sec").</p>
				<p>Lors d'une reprise, les critères suivant d'évaluation seront d'application : (Nombre(s) de propriétaire(s) - Historique du véhicule - Options - Provenance - Kilométrage Certifiés - Contrôle Technique - Carnet d'entretien et/ou Factures d'entretien)</p>
				<p>L'idéal est  de venir nous rendre visite au garage (Chaussée de Waterloo 840 à 1180 Bruxelles ) avec le véhicule, tous les jours sur rendez-vous  afin de vous remettre une offre de prix immédiate.</p>
				<p>A la différence de bon nombre concurrent notre but est vous reprendre votre véhicule mais pas à un prix ridicule, nous évaluerons ensemble le prix idéal de reprise de votre véhicule afin que "tout le monde s'y retrouve" et que vous soyez clients chez nous our de longues années.</p>
				<p>Si vous êtes dans l'impossibilité de vous rendre au garage, vous pouvez nous envoyer un dossier complet de votre véhicule par <a href="mailto:jz@classic-sportscars.be" class="text-blue-500 hover:text-blue-700 hover:underline">e-mail</a>.</p>
			
			@elseif (localization()->getCurrentLocale() == 'nl')
				
				<x-h1 class="pageMainTitle">AANKOOP EN VERKOOP</x-h1>			
				<x-h2>Aankoop</x-h2>

			@elseif (localization()->getCurrentLocale() == 'en')

				<x-h1 class="pageMainTitle">BUYING AND SELLING</x-h1>
				<x-h2>Buying</x-h2>
				<p>Our main interest lies in German sports cars, SUV’s, luxury cars and even classic cars.
				<p>We can also provide a service in which we provide you with the vehicle of your choice in exchange for your older vehicle.</p>
				<p>In the case of our taking your car, the following criterion will be taken into account: Previous owners, car history, specs, origination, certified mileage, roadworthiness tests, service history and bills.</p>
				<p>If you are interested in selling us your car, we suggest you contact us in order for us to set up a meeting in which we can look at the car and give you an offer on the spot.</p>
				<p>Unlike most dealers, our goal is to provide you with the best price possible in order for us to build a longstanding relationship from which both parties benefit.</p>
				<p>If you are unable to meet us for an appraisal you can send us a file on the vehicle via <a href="mailto:jz@classic-sportscars.be" class="text-blue-500 hover:text-blue-700 hover:underline">e-mail</a> and we will give you our best price with the available information.</p>

			@endif
		</div>


		<div class="my-8">
			<livewire:sell-form />
		</div>



		<div>

			@if (localization()->getCurrentLocale() == 'fr')

				<x-h2>Vente</x-h2>
				<p>Notre souci premier est de disposer de véhicules de qualités, révisé et garanti non accidenté à des prix intéressants.</p>
				<p>Tous les véhicules que vous pouvez voir sur notre site sont disponibles directement et visible à notre garage.</p>
				<p>A l'inverse de bon nombre concurrents, nous n'avons pas de véhicules fictifs et pas disponibles. Notre stock est réel et actualisé tous les jours.</p>
				<p>Toutes nos photos ne sont pas retouchées, vous n’aurez pas de mauvaise surprise sur l’état de nos véhicules, vous achetez ce que vous voyez.</p>
				<p>Toutes nos voitures sont rigoureusement sélectionnées, livrées avec doubles des clefs originales, manuels d'utilisation, carnet d'entretien et/ou factures d'entretien, carte grise, certificat de conformité, contrôle technique vente en ordre et Carpass Authentification du kilométrage).</p>
				<p>Nos véhicules sont sévèrement contrôlées  par un atelier et par des mécaniciens qualifiées de la marque et viennent de concessions officielles en Belgique.</p>
				<p>Nous vous laissons l'opportunité de pouvoir faire visiter le véhicule de vos rêves par un expert sélectionné par vos soins.</p>

				<x-h2>Dêpot-vente</x-h2>
				<ul class="list-disc pl-8">
					<li>Estimation gratuite de votre véhicule sur la place ou à domicile en toute discrétion.</li>
					<li>Récupération ou restitution de votre véhicule à votre domicile ou sur le lieu de votre travail.</li>
					<li>Engagement et publication de votre véhicule sur de nombreuses plates formes Internet internationale ce qui facilitera la vente de votre voiture.</li>
					<li>Exposition de votre véhicule si vous le souhaitez dans notre garage ce qui permettra une facilité de vente et un arrêt de vos assurances - taxes de roulage etc...</li>
					<li>Si il y a lieu de faire essayer avec votre véhicule par des candidats-acquéreurs, celui-ci se fera uniquement par nos soins et notre entière responsabilité sur une courte de distance.</li>
					<li>Engagement de revente de votre véhicule dans les 30 jours reconduisibles a prix qui sera défini entre les parties à la signature du mandat de dépôt-vente.</li>
				</ul>
				<p>N'hésitez pas à prendre contact avec nous pour toutes questions complémentaires.</p>
				
				<x-h2>Garantie</x-h2>
				<p>L'ensemble de nos voitures disposent d'une garantie d'1 an Federauto / Traxio  sur les pièces et sur la main d'oeuvre.</p>
				<p>Certains de nos véhicules disposent également d'une garantie constructeur neuve de 2 ans.</p>
				<p>Le véhicule vendu fera l'objet d'un examen approfondi, portant sur 96 points, repris sur un document qui sera remis à l'acheteur le jour de la commande et faisant partie intégrante du contrat. (Contrat de Vente Federauto / Traxio).</p>
				<p>Il y a également une possibilité d'avoir une extension de garantie 2 ans ou 3 ans avec assistance dépannage partout en Europe et véhicule de remplacement moyennant un supplément (voir conditions en nos bureaux).</p>

				<a href="http://www.vendeuragree.be" target="_blank" class="block mt-4" />
					<img src="{{ asset('img/carpass-federauto.png') }}" width="300"/>
				</a>
				
				<x-h2>Export</x-h2>
				<p>Cette rubrique est destinée exclusivement aux personnes venant d'un pays étranger à la Belgique après avoir acheté un véhicule chez nous.</p>
				<p>Une fois le véhicule acheté nous avons la possibilité d'effectuer les démarches administratives pour l'obtention d'une plaque de Transit provisoire belge nominative pour une durée de 1 à 3 mois ainsi qu'une assurance responsabilité civile comprise.</p>
				<p>Cette formule vous permettra de rentrer chez vous en toute légalité.</p>
				
				<x-h2>Livraison</x-h2>
				<p>Nous pouvons également nous charger de la livraison  de votre véhicule partout dans le monde que ce soit par camion ou par bateau et même avion.</p>
				<p>En cas de vente hors C.E nous pouvons livrer le véhicule directement au port d'Anvers ou de Zeebrugge.</p>
				<p>N'hésitez pas à prendre contact avec nous pour toutes questions complémentaires.</p>

				<x-h2>Audité, certifié et contrôlé par Vinçotte.</x-h2>
				<p>La certification de notre entreprise est certainement un point très intéressant. Elle permet de confirmer la transparence du service au client grâce à une société internationale de certification et atteste de sa conformité aux réglementations et prescriptions du secteur, de l’inspection et du contrôle des établissements ainsi que de la formation du vendeur et du conseil donné. La certification est synonyme de contrôles réguliers et constitue une vraie source de qualité et de respect des consommateurs.</p>
	 
				<p>Nous avons d'ailleurs le label de qualité et la certification VENDEUR AGREE FEDERAUTO / TRAXIO</p>
				
				<a href="http://www.vendeuragree.be" target="_blank" class="block my-4"  />
					<img src="{{ asset('img/vendeur-agree.png') }}" width="100"/>
				</a>
					
				<p><strong>Nous restons à votre entière disposition pour toutes questions complémentaires et nous serons ravi de pouvoir réaliser votre rêve.</strong></p>

			@elseif (localization()->getCurrentLocale() == 'nl') 
				
				<x-h2>Verkooppunt</x-h2>
				<ul class="list-disc pl-8">
					<li>gratis overname-offerte voor uw voertuig ter plaatse bij ons of bij U, in alle discretie</li>
					<li>afhalen/terugbezorgen van het voertuig bij U thuis of op uw werk</li>
					<li>Publicatie van uw voertuig in verschillende media om de verkoop te stimuleren</li>
					<li>op uw aanvraag stellen we het voertuig ook tentoon in onze garage. Dit laat U tevens toe om de betalingen aangaande verzekeringen en belastingen te kunnen stopzetten</li>
					<li>testritten voor eventuele kopers gebeuren steeds door ons en onder onze volledige verantwoordelijkheid en dit voor zeer korte afstanden</li>
					<li>wij engageren er ons toe om het voertuig binnen de 30 dagen(verlengbaar)na ondertekening van het contract, te verkopen aan de onderhandelde prijs Aarzel niet om ons te contacteren indien u meer informatie wenst</li>
				</ul>

				<x-h2>Verkoop</x-h2>
				<p>Onze grootste zorg is de beschikbaarheid van kwalitatieve voertuigen die we met garantie en aan eerlijke prijzen kunnen aanbieden.</p>
				<p>De door ons aangeboden voertuigen die u kan terugvinden op deze site, zijn onmiddellijk beschikbaar en te bezichtigen na afspraak in onze garage.</p>
				<p>Deze wagens werden zorgvuldig geselecteerd en worden geleverd met alle nodige documenten, handleidingen, certificaten, onderhoudsboekje en –facturen, de originele sleutels, en zijn voorzien van een blanco keuringsverslag en CARPASS(kilometerstand controle) document.</p>
				<p>Tevens werd elk voertuig uitgebreid gecontroleerd door een gekwalificeerd technieker van het eigen merk.</p>
				<p>Wij bieden u tevens de mogelijkheid om het voertuig te laten controleren door een door u aangestelde technieker en/of specialist.</p>
				
				<x-h2>Garantie</x-h2>
				<p>Alle voertuigen worden geleverd met 1 jaar garantie op wisselstukken en werkuren.</p>
				<p>Sommige wagens genieten van een tweejarige constructeursgarantie.</p>
				<p>Het aangeboden voertuig zal een extra controle op 96 verschillende punten ondergaan. Het rapport hiervan wordt U overhandigd wanneer het u het voertuig aanschaft (Verkoopcontract Federauto).</p>
				<p>We bieden u tevens de mogelijkheid tot een garantie-uitbreiding van 2 of 3 jaar, inclusief sleepkosten over heel Europa mits de betaling van een supplement (voorwaarden ter inzage in onze burelen).</p>

				<a href="http://www.http://www.erkendverkoper.be" target="_blank" class="block my-4" />
					<img src="{{ asset('img/carpass-federauto.png') }}" width="300"/>
				</a>
				
				<x-h2>Export</x-h2>
				<p>Enkel beschikbaar voor personen met andere dan de Belgische nationaliteit die een voertuig aankochten bij ons.</p>
				<p>Eens het contract ondertekend ondernemen wij de nodige administratie ten onze laste. Wij bezorgen U transit-platen voor 1 tot 3 maanden en de verzekeringsdocumenten inclusief dekking Burgerlijke aansprakelijkheid.</p>
				<p>Op die manier kan u in alle rust en legaal met het voertuig huiswaarts keren.</p>
				<p>Op afspraak kunnen wij U ook afhalen in de verschillende nabij gelegen stations van de Brusselse hoofdstad(Zuid/Centraal) of de lokale luchthavens (Zaventem/Charleroi) teneinde extra vervoerskosten te beperken.</p>
				
				<x-h2>Levering</x-h2>
				<p>Wij kunnen eventueel ook zorgen voor de aflevering van het voertuig over de hele wereld. Hetzij per vrachtwagen, boot of zelf vliegtuig. Ook bij verkoop buiten de Europese Unie kunnen wij zorgen voor een levering in de haven van ZeeBrugge of Antwerpen.</p>

			@elseif (localization()->getCurrentLocale() == 'en')
		
				<x-h2>Selling</x-h2>
				<p>Our goal is to sell high quality vehicles that have been fully serviced and have a continuous history at the best prices on the market. We avoid any cars that have been crashed in the past to guarantee the quality of our stock.</p>
				<p>All items in our stock are available immediately and are available for a viewing at our garage simply by arranging an appointment with a member of our team.
				<p>In contrast to many competitors, we do not have a fictitious stock or unavailable vehicles. Our entire range of vehicles is updated every-day to avoid disappointment.</p>
				<p>We make it our objective to select our range of cars very specifically and provide them with their two sets of keys, all the available paper work, service history and bills as well as registration information and certification of mileage.</p>
				<p>Our stock is rigorously controlled by an independent workshop as well as certified brand technicians (Mercedes, Audi, etc.).</p>
				<p>Please visit the current selections of available vehicles; we have worked hard to provide you with the best on the market for your pleasure.</p>

				<x-h2>Deposit - Sale</x-h2>
					<ul class="list-disc pl-8">
						<li>Free appraisal of your vehicle at a location of your choice, in perfect confidentiality.</li>
						<li>Return of your vehicle and your home or place of work.</li>
						<li>We will publicize the sale of your vehicle on our website as well as other internet platforms in order to provide you with the best conditions of sale and the most publicity possible.</li>
						<li>Display of your vehicle –at your discretion- in order to ease the process of sale and avoid continuation of insurance and other payments.</li>
						<li>Any trial of your vehicle by a potential client will be done under our full responsibility on very short distances.</li>
						<li>We will commit to sell your vehicles within 30 days at a price agreed upon at the signing of the warrant-of-sale.</li>
					</ul>
					<p>Please contact us with any further questions you may have concerning our methods.</p>

				<x-h2>Warranty</x-h2>
				<p>All of our cars are sold with a 1-year warranty on all parts and man-hours. A selection of vehicles comes with a 2-year warranty provided by their constructors. Any vehicle sold by us will undergo a thorough examination taking 96 factors into account all of which are compiled on a document provided to the buyer at time of purchase; this is an integral part of our service and is therefore part of the warrant-of-purchase (Federauto contract).</p>
				<p>We can also provide any buyer with a 2 or 3-year extension of the warranty as well as road-assistance and a replacement vehicles. (Conditions apply: see details at our offices).</p>

				<a href="http://www.http://www.erkendverkoper.be" target="_blank" class="block my-4" />
					<img src="{{ asset('img/carpass-federauto.png') }}" width="300"/>
				</a>
				
				<x-h2>Export to outside Belgium</x-h2>
				<p>This section concerns those clients wishing to purchase one of our vehicles and exporting it.</p>
				<p>After having purchased a vehicle from our stock, we can provide you with a transit plate valid for 1 to 3 months as well as the insurance necessary to drive the vehicle. This will allow you to bring the vehicle to your home (outside Belgium) at the wheel!</p>
				<p>We are also able to pick you up, at our expense, at local train stations (Gare Du Midi, Gare Centrale) and airports (Zaventem, Charleroi).</p>
				
				<x-h2>Delivery</x-h2>
				<p>We can deliver your new vehicle any where in the world, be it by boat, plane or truck. In case of a sale outside of the E.U we can deliver your vehicle directly to the Antwerp or Zeebrugge port.</p>
		
			@endif
		</div>
		
	</section>
 
@endsection