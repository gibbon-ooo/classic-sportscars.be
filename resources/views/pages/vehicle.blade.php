@extends('layouts.app')

@section('description')
	@if (localization()->getCurrentLocale() == 'fr')
	@elseif (localization()->getCurrentLocale() == 'nl')
	@elseif (localization()->getCurrentLocale() == 'en')
	@endif
@endsection

@section('title', trans("messages.our-vehicles-title"))

@section('content')

<section class="px-4 md:px-8 py-12 md:py-20 max-w-7xl mx-auto">


	<div class="mb-8">
		<div class="ml-4">
			<?php
				$status = "beschikbaar";
				$color = "bg-gray-800";
				if ($car->has_option) {
					$status = "optie";
					$color = "bg-orange-500";
				} elseif ($car->sold) {
					$status = "verkocht";
					$color = "bg-red-500";
				}
			?>

			@if($status === "optie")
				<span class="bg-orange-500 p-2 rounded-t-md text-white">{{ trans('messages.option') }}</span>
			@elseif($status === "verkocht")
				<span class="bg-red-500 p-2 rounded-t-md text-white">{{ trans('messages.sold') }}</span>
			@endif
		</div>

        <div class="-mt-12 pt-8 relative">
            <div class="{{ $color }} rounded-md text-xl md:text-2xl font-bold text-white py-4 px-8 flex flex-col md:flex-row space-y-4 md:space-y-0 md:justify-between md:items-center {{$status}}">
                <div>
                    <h1>{{ $car->title }}</h1>
                    <p>{{ $car->subtitle }}</p>
                </div>

                <p class="text-gray-300">
                    @if($car->is_vat_vehicle)
                        &euro; {{ $car->netto_price }} <span class="text-sm text-gray-400">{{ trans('messages.vat-excluded') }}</span>
                    @else
                        &euro; {{ $car->price }} <span class="text-sm text-gray-400">{{ trans('messages.vat-included') }}</span>
                    @endif
                </p>

                @if($car->is_vat_vehicle)
                    <div class="absolute top-0 right-0 -mr-2 md:-mr-4 rotate-12 text-white bg-blue-500 rounded-lg p-2 text-base md:text-lg">
                        {{ trans('messages.vat-vehicle') }}
                    </div>
                @endif
            </div>
        </div>


	</div>

	<div class="flex flex-col md:flex-row md:flex-wrap">

		<div class="w-full md:w-3/4 pr-4">
			<div class="grid grid-cols-1 sm:grid-cols-2 sm:gap-4">
				<div>
					<p class="border-b border-gray-200 py-2"><span class="car-eigenschap">
                        <span class="font-bold">{{ trans('messages.first-registration') }}</span>: @if($car->first_inscription_date != '0000-00-00') {{ $car->first_inscription_date }} @endif
                    </p>

                    <p class="border-b border-gray-200 py-2">
                        <span class="font-bold">{{ trans('messages.vat-recoverable') }}</span>: {{ $car->is_vat_vehicle ? trans('messages.yes') : trans('messages.no') }}
                    </p>

                    @if ($car->is_vat_vehicle)
                    <p class="border-b border-gray-200 py-2">
                        <span class="font-bold">{{ trans('messages.gross-price') }}</span>: {{ $car->price ? '€ ' . $car->price : '-' }}
                    </p>

                    <p class="border-b border-gray-200 py-2">
                        <span class="font-bold">{{ trans('messages.netto-price') }}</span>: {{ $car->netto_price ? '€ ' . $car->netto_price : '-' }}
                    </p>
                    @endif

					<p class="border-b border-gray-200 py-2">
						<span class="font-bold">CO2</span>: {{ $car->co2 ? $car->co2 . ' g' : '-' }}
					</p>

                    <p class="border-b border-gray-200 py-2">
                        <span class="font-bold">{{ trans('messages.horsepower') }}</span>: {{ $car->horsepower ?? '-' }}
                    </p>

                    @if (!$car->is_vat_vehicle)
					<p class="border-b border-gray-200 py-2">
						<span class="font-bold">{{ trans('messages.color') }}</span>: @if ($car->color) {{ $car->color->name }} @else - @endif
					</p>
                    @endif
				</div>
				<div>
                    @if ($car->is_vat_vehicle)

                        <p class="border-b border-gray-200 py-2">
                            <span class="font-bold">{{ trans('messages.color') }}</span>: @if ($car->color) {{ $car->color->name }} @else - @endif
                        </p>
                    @endif

					<p class="border-b border-gray-200 py-2">
						<span class="font-bold">{{ trans('messages.mileage') }}</span>: {{ $car->kilometers ? $car->kilometers . ' km' : '-' }}
					</p>
					<p class="border-b border-gray-200 py-2">
						<span class="font-bold">{{ trans('messages.gearing-type') }}</span>: @if ($car->transmission) {{ $car->transmission->name }} @else - @endif
					</p>
					<p class="border-b border-gray-200 py-2">
						<span class="font-bold">{{ trans('messages.fuel') }}</span>: @if ($car->fuel) {{ $car->fuel->name }} @else - @endif
					</p>
                    <p class="border-b border-gray-200 py-2">
                        <span class="font-bold">{{ trans('messages.pollution-standard') }}</span>: {{ $car->pollution_standard ?? '-' }}
                    </p>
				</div>

				<div class="mt-4">
					<h2 class="text-2xl font-bold mb-4">{{ trans('messages.description') }}</h2>
					{!! $car->description !!}
				</div>
			</div>
		</div>

		<div class="w-full md:w-1/4 mt-8 md:mt-0">
			<div class="grid grid-cols-2 gap-4">
				@foreach($car->getMedia('photos')->sortBy('order_column') as $media)
					<a href="{{ $media->getUrl('full') }}" data-fslightbox="car-images" data-type="image">
					 	<img class="rounded-md" src="{{ $media->getUrl('thumb') }}" alt=""/>
					</a>
				@endforeach
			</div>
		</div>

	</div>

</section>

@endsection

@section('pagescripts')
<script src="{{ asset('/js/fslightbox.js') }}"></script>

fsLightbox.props.type = 'image';
@endsection
