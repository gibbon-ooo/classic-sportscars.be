@extends('layouts.app')

@section('description')
	@if (localization()->getCurrentLocale() == 'fr')
	@elseif (localization()->getCurrentLocale() == 'nl')
	@elseif (localization()->getCurrentLocale() == 'en')
	@endif
@endsection

@section('title', trans("messages.our-vehicles-title"))

@section('content')


<section class="px-4 md:px-8 py-12 md:py-20 max-w-7xl mx-auto">

	<livewire:cars-filter />

</section>
       
@endsection