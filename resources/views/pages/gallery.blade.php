@extends('layouts.app')

@section('description')
	@if (localization()->getCurrentLocale() == 'fr')
	@elseif (localization()->getCurrentLocale() == 'nl')
	@elseif (localization()->getCurrentLocale() == 'en')
	@endif
@endsection

@section('title', trans("messages.gallery-title"))

@section('content')
    
    <section class="px-4 md:px-8 py-12 md:py-20 max-w-7xl mx-auto">
		
		<h1 class="text-xl md:text-2xl font-medium mb-4 md:mb-8 font-open-sans uppercase">
    		@fr
    			Voici un éventail des dernières voitures vendues
    		@elsenl
    			Een overzicht van onze reeds verkochte voertuigen
    		@elseen
    			Discover an overview of our latest sold cars
    		@endfr
		</h1>
    	
    	<div class="grid gap-4 md:gap-4 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
            @php
	            $files = \Arr::sort(\File::allFiles(storage_path('app/public/gallery')), function($file)
	                {
	                    return $file->getFilename();
	                });
            @endphp

	        @foreach ($files as $file)
	        	<div class="p-0 overflow-hidden rounded-2xl">
	        		<img src="{{ asset('/gallery/'.$file->getFilename()) }}" alt="" class="scale-110">
	        	</div>
	        @endforeach
	    </div>

	</section>      
@endsection