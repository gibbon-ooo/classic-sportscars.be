@extends('layouts.app')

@section('description')
	@if (localization()->getCurrentLocale() == 'fr')
	@elseif (localization()->getCurrentLocale() == 'nl')
	@elseif (localization()->getCurrentLocale() == 'en')
	@endif
@endsection

@section('title', trans("messages.who-we-are-title"))

@section('seo')
	@if (localization()->getCurrentLocale() == 'fr')
		<meta name="description" content="Classic and Sportscars est principalement à la recherche de véhicules allemands de sports, de prestiges, 4X4 , S.U.V et même ancêtres.">
	@elseif (localization()->getCurrentLocale() == 'nl')
		<meta name="description" content="Wij zijn hoofdzakelijk op zoek naar Duitse sport – en prestigewagens, 4×4, S.U.V. en zelf old-timers.">
	@elseif (localization()->getCurrentLocale() == 'en')
		<meta name="description" content="We are driven by our passion for automobiles. We have made it our primary goal to provide our customers with very special and high quality vehicles that come directly from specific and passionate collectors.">
	@endif
@endsection

@section('content')
<div class="p-4 px-4 md:px-8 py-12 md:py-20">
    <section class="max-w-7xl mx-auto">
			
		@fr
			<x-h1 class="text-xl font-bold font-open-sans mb-4 uppercase">QUI SOMMES NOUS</x-h1>
			<div class="bg-gray-100 border border-gray-200 p-4 rounded-xl">
				<p>Passionnés d'automobiles,</p>
				<p>Classic and Sportscars a été fondé par deux entrepreneurs associé, James-Oliver Moyses s’occupant intégralement des ventes et du service après-vente et le second Julian Zenner s’occupant des achats.</p>
				<p>Nous nous sommes fixé pour objectif d'offrir des véhicules triés sur le volet venant majoritairement de concessions officielles en Belgique et d'une communauté de collectionneurs,passionnés toutes munies d'un historique limpide et sans faille.</p>
				<p>Dans le monde privé des passionnées c'est installé une véritable envie commune de transparence afin de redonner ces lettres de noblesse a une profession trop souvent éreinté par des marchands peux scrupuleux.</p>
				<p>Plus qu'une passion le monde de l'excellence automobile est devenu avec les années un art de vivre alliant des ancêtres jusqu'aux technologies de pointes.</p>
				<p>Classic and Sportscars garantit de vous offrir une large gamme de véhicules scrupuleusement vérifiés munis de leurs historiques complets.</p>
				<p>Nous voulons absolument avoir une transparence totale vis à vis de nos clients et garantir des produits de qualités et construisions une réputation que nous voulons perdurer pendant des années.</p>
			</div>

		@elsenl

			<x-h1 class="text-xl font-bold font-open-sans mb-4 uppercase">WIE ZIJN WIJ</x-h1>
			<div class="bg-gray-100 border border-gray-200 p-4 rounded-xl">
				<p>Autoliefhebbers,</p>
				<p>Classic and Sportscars werd opgericht door twee ondernemers, James-Oliver Moyses wie instaat voor de verkoop en Julian Zenner, wie instaat voor de aankoop.</p>
				<p>In het milieu van gepassioneerde en liefhebbers stellen we vast dat er een grote vraag is naar transparantie en duidelijkheid omtrent de herkomst van de voertuigen, daar waar de huidige markt vergeven is van handelaars die maar al te graag minderwaardige voertuigen te koop aanbieden.</p>
				<p>Meer nog dan een passie is het door de jaren heen een kunst geworden om de link te leggen tussen oldtimers en de laatste nieuwe technieken.</p>
				<p>Classic en Sportscars tracht U een uitgebreid gamma aan wagens aan te bieden, wagens die tot in detail werden gecontroleerd en geverifieerd doorheen hun bestaansgeschiedenis.</p>
			</div>

		@elseen

			<x-h1 class="text-xl font-bold font-open-sans mb-4 uppercase">ABOUT US</x-h1>
			<div class="bg-gray-100 border border-gray-200 p-4 rounded-xl">
				<p>Auto enthusiasts,</p>
				<p>Classic and Sportscars was create by two associate first James Oliver Moyses involved in sales and after-sales and the second Julian Zenner dealing with purchases.</p>
				<p>We notice more and more that there is a strong demand for transparency and clarity about the origin of second hand vehicles, where the most of the current market dealers do not offer the service you deserve.</p>
				<p>Over the years it became an art to make the link between old-timers and the latest techniques used in car technology.</p>
				<p>Classic and Sportscars endeavors to offer you a wide range of cars, cars who were checked in every detail and verified through their existence history.</p>
			</div>
		
		@endfr


	    <div class="flex flex-col md:flex-row md:flex-wrap mt-8 md:mt-12">	
	    	
	    	<div class="w-full md:w-1/2 pr-0 md:pr-4">
	    	
		    	@fr
				
					<p>Nous sommes principalement à la recherche de véhicules allemands de sports, de prestiges, 4X4 , S.U.V et même ancêtres.</p>
					<p>C'est dans ce cadre que nous vous proposons l'éventuelle reprise de votre ancien véhicule, que vous nous achetiez une voiture ou non (achat "sec").</p>
					<p>Lors d'une reprise, les critères suivant d'évaluation seront d'application : (Nombre(s) de propriétaire(s) - Historique du véhicule - Options - Provenance - Kilométrage Certifiés - Contrôle Technique - Carnet d'entretien et/ou Factures d'entretien).</p>
					<p>L'idéal est de venir nous rendre visite au garage (Avenue de la Forêt de Soignes 155 à Rhode St Genèse ) avec le véhicule, tous les jours sur rendez-vous afin de vous remettre une offre de prix immédiate.</p>
					<p>A la différence de bon nombre concurrent notre but est vous reprendre votre véhicule mais pas à un prix ridicule, nous évaluerons ensemble le prix idéal de reprise de votre véhicule afin que "tout le monde s'y retrouve" et que vous soyez clients chez nous our de longues années.</p>
					<p>Si vous êtes dans l'impossibilité de vous rendre au garage, vous pouvez nous envoyer un dossier complet de votre véhicule par <a href="mailto:jz@classic-sportscars.be" class="text-blue-500 hover:text-blue-700 hover:underline">e-mail</a>.</p>
					<p>Audité, certifié et contrôlé par Vinçotte.</p>
					<p>La certification de notre entreprise est certainement un point très intéressant. Elle permet de confirmer la transparence du service au client grâce à une société internationale de certification et atteste de sa conformité aux réglementations et prescriptions du secteur, de l’inspection et du contrôle des établissements ainsi que de la formation du vendeur et du conseil donné. La certification est synonyme de contrôles réguliers et constitue une vraie source de qualité et de respect des consommateurs.</p>
					<p>Nous avons d’ailleurs le label de qualité et la certification VENDEUR AGREE FEDERAUTO.</p>
					<p>Nous restons à votre entière disposition pour toutes questions complémentaires et nous serons ravi de pouvoir réaliser votre rêve.</p>
					<br>
					<p>James-Oliver Moyses et Julian Zenner<br>
					Administrateurs</p>

				@elsenl
					
					<p>Wij zijn hoofdzakelijk op zoek naar Duitse sport – en prestigewagens, 4×4, S.U.V. en zelf old-timers.</p>
					<p>Wij zijn steeds bereid om binnen deze selectie van voertuigen een offerte te maken voor de overname van uw voertuig, of U nu al dan niet bij ons een nieuw voertuig aanschaft.</p>
					<p>Bepaalde criteria zijn van toepassing bij een overname: aantal vorige eigenaars, historiek van het voertuig, opties, afkomst, kilometerstand, technische controle, onderhoudsboekje en/of facturen.</p>
					<p>Idealiter komt u met het voertuig naar onze vestiging (op afspraak op het adres Zoniënwoudlaan 155 te Sint Genesius Rode) teneinde u ter plaatse een offerte te kunnen maken.</p>
					<p>Wij trachten onze concurrenten een stap voor te zijn door U, in overleg, een degelijke prijs voor Uw voertuig te bieden. Een prijs waarin beide partijen zich kunnen vinden en die ons en U toelaat om een trouw cliënteel op te bouwen.</p>
					<p>Indien u niet ter plaatse kan komen kan u ons steeds een uitgebreid dossier overmaken van het voertuig per e-mail.</p>
					<p>Door Vinçotte aan een audit onderworpen, gecertificeerd en gecontroleerd. De certificering van de onderneming is zeker een zeer interessant punt, aangezien zo de transparantie van de dienstverlening aan de klant bevestigd wordt dankzij een internationale firma.</p>
					<p>De certificering bewijst tevens dat de onderneming in overeenstemming is met de voorschriften en reglementeringen van de sector en dat de vestiging gecontroleerd engeïnspecteerd werd evenals dat de verkoper opgeleid is en advies gekregen heeft. De certificering staat synoniem voor regelmatige controles, kwaliteit en respect voor de consument.</p>
					<p>Wij staan ter uwer beschikking voor alle bijkomstige vragen en hopen Uw droom te kunnen realiseren.</p>
					<br>
					<p>James-Oliver Moyses et Julian Zenner<br>
					Zaakvoerders</p>

				@elseen
					
					<p>Audited certified and controlled by Vinçotte.</p>
					<p>The certification of our business is certainly a very interesting point. It confirms transparency of customer service through a global certification and certify its compliance with industry regulations and requirements: inspection and monitoring of facilities and training of the seller and the advice given.</p>
					<p>Certification is synonymous with regular checks and is a true source of quality and respect for consumers.</p>
					<p>We also label quality and certification AGREED AND OFFICIAL FEDERAUTO SELLER.</p>
					<p>We are at your service for any questions regarding our service or range of luxury cars.</p>
					<p>Our goal is to share the passion we have for automobile and help make your dream come true.</p>
					<br>
					<p>James-Oliver Moyses et Julian Zenner<br>
					Managers</p>

				@endfr

				<img src="{{ asset('img/james-oliver.jpg')}}" width="250" class="block mt-8 rounded-2xl" />

			</div>

				
			<div class="w-full md:w-1/2 pl-0 md:pl-4 mt-12 md:mt-0">
				      
				<a href="{{ localization()->getUrlFromRouteName(localization()->getCurrentLocale(), 'routes.gallery') }}">
					<div x-data 
		                 x-init="() => {
		                            var glide = new Glide($el, {
		                              type: 'carousel',
		                              autoplay: 2000,
		                              animationDuration: 1000,
		                              hoverpause: false,
		                              gap: 0,
		                            });
		                            glide.mount()
		                        }"
		                class="rounded-2xl overflow-hidden"
		            >
		                <div class="glide__track" data-glide-el="track">
		                    <ul class="glide__slides">
		                    	@foreach ( \File::allFiles(public_path('gallery-garage')) as $file)
						        	<li class="glide__slide">
			                            <img src="{{ asset('/gallery-garage/'.$file->getFilename()) }}">
			                        </li>					
						        @endforeach
		                    </ul>
		                </div>
		            </div>
		        </a>

			</div>

		</div>

    </section>
</div>    
@endsection