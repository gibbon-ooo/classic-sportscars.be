@extends('layouts.app')

@section('content')

    <section class="px-4 md:px-8 py-12 md:py-20 max-w-7xl mx-auto">

        <h1 class="text-xl md:text-2xl font-medium mb-2 md:mb-4 font-open-sans">BIENVENUE CHEZ CLASSIC & SPORTSCARS</h1>

        <p class="text-sm md:text-base">Depuis 2007 nous sommes spécialisé dans le marché d’achat et vente de véhicules ce qui nous permettent de vous trouver la perle rare grâce à notre réseau et à notre collaboration avec les concessions officielles de Belgique, ceci afin de vous éviter tous les pièges liés à l’occasion et vous offrir un véhicule d’une qualité exceptionnelle.</p>

        <p class="text-sm md:text-base">Nos véhicules sont garantis, révisés et disponibles immédiatement.</p>

        <p class="text-sm md:text-base">De plus, nos contacts journaliers à travers diverses concessions et clients nous permettent de gérer un stock important de véhicules quasi-neufs et ou d’occasion.</p>

        <p class="text-sm md:text-base">Nos voitures sont visibles tous les jours, y compris le samedi matin.</p>

        <p class="text-sm md:text-base">N’hésitez pas à nous contacter pour toutes autres questions complémentaires.</p>

    </section>

    <livewire:cars-index />
    
    <section class="max-w-7xl mx-auto py-12">
        <div class="flex flex-nowrap space-x-4 overflow-x-scroll pb-8">
            @foreach(\App\Models\Brand::orderBy('name')->get() as $brand)
            <a href="{{ localization()->getUrlFromRouteName(localization()->getCurrentLocale(), 'routes.our-vehicles') }}?b={{ $brand->id }}">
                <div x-data x-tooltip="{{ $brand->name }}" class="w-24 h-24 flex items-center justify-center">
                    <img src="/storage/{{ $brand->icon }}" class="h-20 w-auto" />
                </div>
            </a>
            @endforeach
        </div>
    </section>




    <section class="max-w-5xl mx-auto bg-gray-100 border border-gray-200 md:rounded-2xl p-4 md:p-8 flex flex-col sm:flex-row sm:flex-wrap">
        <div class="w-full sm:w-1/2">
            @fr
                <p><strong>Heure d’ouverture:</strong></p>
                <p><strong>Lundi au Vendredi de 9h00 à 18h00 sans interruption.</strong></p>
                <p><strong>Le Samedi matin de 10h00 à 13h00.</strong></p>
                <p><strong>Fermé le dimanche et jour férié.</strong></p>
                <br>                            
                <p>TEL: <a href="tel:+3223745015">+32 (0)2-374 50 15</a></p>
                <p>FAX: +32 (0)2-374 18 81</p>
                <p>MAIL: <a href="mailto:info@classic-sportscars.be">info@classic-sportscars.be</a></p>
                <br>
                <p>Adresse:</p>
                <p>155 Avenue de la Fôret de Soignes<br>
                1640 Rhode St Genèse (Belgique)</p>
                <br>
                <p>TVA: BE 0821 263 366</p>
            @elsenl
                <p><strong>Openingsuren:</strong></p>
                <p><strong>Maandag tot vrijdag doorlopend van 9u00 tot 18u00.</strong></p>
                <p><strong>Zaterdagvoormiddag van 10u00 tot 13u00.</strong></p>
                <p><strong>Gesloten op zondag en feestdagen.</strong></p>
                <br>    
                <p>TEL: <a href="tel:+3223745015">+32 (0)2-374 50 15</a></p>
                <p>FAX: +32 (0)2-374 18 81</p>
                <p>MAIL: <a href="mailto:info@classic-sportscars.be">info@classic-sportscars.be</a></p>
                <br>
                <p>Adres:</p>
                <p>Zoniënwoudlaan 155<br>
                1640 Sint Genesius Rode (België)</p>
                <br>
                <p>BTW: BE 0821 263 366</p>
            @elseen
                <p><strong>Opening hours:</strong></p>
                <p><strong>Mondat to friday from van 9h00 to 18h00.</strong></p>
                <p><strong>Saturday morning from 10h00 to 13h00.</strong></p>
                <p><strong>Closed on sundays and holidays.</strong></p>
                <br>
                <p>TEL: <a href="tel:+3223745015">+32 (0)2-374 50 15</a></p>
                <p>FAX: +32 (0)2-374 18 81</p>
                <p>MAIL: <a href="mailto:info@classic-sportscars.be">info@classic-sportscars.be</a></p>
                <br>
                <p>Address:</p>
                <p>Zoniënwoudlaan 155<br>
                1640 Sint Genesius Rode (Belgium)</p>
                <br>
                <p>VAT: BE 0821 263 366</p>
            @endfr
        </div>

        <div class="w-full sm:w-1/2">

            <?php 
                $files = \Arr::sort(\File::allFiles(public_path('gallery-home')), function($file)
                {
                    return $file->getFilename();
                });
            ?>

            <a href="{{ localization()->getUrlFromRouteName(localization()->getCurrentLocale(), 'routes.gallery') }}">
                <div x-data 
                     x-init="() => {
                                var glide = new Glide($el, {
                                  type: 'carousel',
                                  autoplay: 2000,
                                  animationDuration: 1000,
                                  hoverpause: false,
                                  gap: 0,
                                });
                                glide.mount()
                            }"
                >
                    <div class="glide__track" data-glide-el="track">
                        <ul class="glide__slides">
                            @foreach ($files as $file)
                            <li class="glide__slide">
                                <img src="{{ asset('/gallery-home/'.$file->getFilename()) }}" class="rounded-2xl">
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </a>

        </div>

    </section>


@endsection



@section('pagescripts')
    <script>
        /*
        var sliders = document.querySelectorAll('.glide');

        for (var i = 0; i < sliders.length; i++) {
          var glide = new Glide(sliders[i], {
            gap: 15,
          });
          
          glide.mount();
        }
        */
    </script>
@endsection