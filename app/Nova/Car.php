<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Boolean;
use Illuminate\Database\Eloquent\Builder;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasMany;
use Ebess\AdvancedNovaMediaLibrary\Fields\Files;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;

class Car extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Car::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name_en';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            BelongsTo::make('Brand')->rules('required'),

            Text::make('Title NL')->rules('required')->hideFromIndex(),
            Text::make('Title FR')->rules('required')->hideFromIndex(),
            Text::make('Title EN')->rules('required'),

            Text::make('Subtitle NL')->rules('max:255')->hideFromIndex(),
            Text::make('Subtitle FR')->rules('max:255')->hideFromIndex(),
            Text::make('Subtitle EN')->rules('max:255')->hideFromIndex(),

            Trix::make('Description NL')->hideFromIndex(),
            Trix::make('Description FR')->hideFromIndex(),
            Trix::make('Description EN')->hideFromIndex(),

            BelongsTo::make('Color')->hideFromIndex(),

            BelongsTo::make('Transmission')->hideFromIndex(),

            BelongsTo::make('Fuel')->hideFromIndex(),

            Text::make('Price (€)', 'price')->hideFromIndex(),
            Text::make('Netto Price (€)', 'nett_price')->hideFromIndex(),

            Text::make('First inscription date')->hideFromIndex(),

            Text::make('Kilometers')->hideFromIndex(),

            Number::make('CO2')->hideFromIndex(),

            Text::make('Pollution standard', 'pollution_standard')->hideFromIndex(),

            Text::make('Horsepower')->hideFromIndex(),

            Boolean::make('Is active', 'is_active')->hideFromIndex(),

            Boolean::make('Is VAT vehicle', 'is_vat_vehicle')->hideFromIndex(),

            Boolean::make('Is sold', 'is_sold')->hideFromIndex(),

            Boolean::make('Option', 'has_option')->hideFromIndex(),

            Boolean::make('In the picture', 'is_featured')->hideFromIndex(),

            Images::make('Photos', 'photos') // second parameter is the media collection name
                ->conversionOnPreview('full') // conversion used to display the "original" image
                ->conversionOnDetailView('thumb') // conversion used on the model's view
                ->conversionOnIndexView('thumb') // conversion used to display the image on the model's index page
                ->conversionOnForm('thumb') // conversion used to display the image on the model's form
                ->fullSize() // full size column
                ->singleImageRules('dimensions:min_width=1200,mimes:jpeg'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
