<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SellForm extends Mailable
{
    use Queueable, SerializesModels;
    
    public $data;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail = $this->subject("Reprise de véhicule")->markdown('emails.sell-form');

        if ($this->data['file'] !== null) {
            $mail = $mail->attach($this->data['file']->getRealPath(), array(
                'as' => 'file.' . $this->data['file']->getClientOriginalExtension(), 
                'mime' => $this->data['file']->getMimeType())
            );
        }

        return $mail;
    }
}