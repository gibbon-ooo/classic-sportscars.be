<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::if('nl', function () {
            return localization()->getCurrentLocale() === 'nl';
        });

        Blade::if('fr', function () {
            return localization()->getCurrentLocale() === 'fr';
        });

        Blade::if('en', function () {
            return localization()->getCurrentLocale() === 'en';
        });
    }
}
