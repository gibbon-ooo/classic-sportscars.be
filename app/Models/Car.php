<?php

namespace App\Models;

use Illuminate\Support\Str;
use Spatie\Image\Manipulations;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Car extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    public static function boot() {
        parent::boot();

        static::creating(function ($model) {        
            $slug_nl = Str::slug(str_replace('/', '-', $model->title_nl), '-');
            $slugCount_nl = count(Car::whereRaw("slug_nl REGEXP '^{$slug_nl}(-[0-9]*)?$'")->get() );
            $model->slug_nl = ($slugCount_nl > 0) ? "{$slug_nl}-{$slugCount_nl}" : $slug_nl;

            $slug_fr = Str::slug(str_replace('/', '-', $model->title_fr), '-');
            $slugCount_fr = count(Car::whereRaw("slug_fr REGEXP '^{$slug_fr}(-[0-9]*)?$'")->get() );
            $model->slug_fr = ($slugCount_fr > 0) ? "{$slug_fr}-{$slugCount_nl}" : $slug_fr;

            $slug_en = Str::slug(str_replace('/', '-', $model->title_en), '-');
            $slugCount_en = count(Car::whereRaw("slug_en REGEXP '^{$slug_en}(-[0-9]*)?$'")->get() );
            $model->slug_en = ($slugCount_en > 0) ? "{$slug_en}-{$slugCount_en}" : $slug_nl;            

        });

        static::updating(function ($model) {        
            if($model->isDirty('title_nl')){
                $slug = Str::slug(str_replace('/', '-', $model->title_nl), '-');
                $slugCount = count(Car::whereRaw("slug_nl REGEXP '^{$slug}(-[0-9]*)?$'")->get() );
                $model->slug_nl = ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
            }

            if($model->isDirty('title_fr')){
                $slug = Str::slug(str_replace('/', '-', $model->title_fr), '-');
                $slugCount = count(Car::whereRaw("slug_fr REGEXP '^{$slug}(-[0-9]*)?$'")->get() );
                $model->slug_fr = ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
            }

            if($model->isDirty('title_en')){
                $slug = Str::slug(str_replace('/', '-', $model->title_en), '-');
                $slugCount = count(Car::whereRaw("slug_en REGEXP '^{$slug}(-[0-9]*)?$'")->get() );
                $model->slug_en = ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
            }
            
        });
    }

    public function registerMediaCollections(): void 
    {
        $this->addMediaConversion('thumb')
              ->width(400)
              ->watermark( public_path('img/watermark_thumb.png') )
              ->watermarkPosition(Manipulations::POSITION_BOTTOM_RIGHT)
              ->watermarkPadding(15)
              ->sharpen(10);

        $this->addMediaConversion('full')
              ->width(1200)
              ->watermark( public_path('img/watermark_full.png') )
              ->watermarkPosition(Manipulations::POSITION_BOTTOM_RIGHT)
              ->watermarkPadding(30)
              ->sharpen(10);
    }

    public function getSlugAttribute()
    {
        return $this['slug_'.localization()->getCurrentLocale()];
    }

    public function getTitleAttribute()
    {
        return $this['title_'.localization()->getCurrentLocale()];
    }

    public function geSubTitleAttribute()
    {
        return $this['subtitle_'.localization()->getCurrentLocale()];
    }

    public function getDescriptionAttribute()
    {
        return $this['description_'.localization()->getCurrentLocale()];
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function color()
    {
        return $this->belongsTo(Color::class);
    }

    public function transmission()
    {
        return $this->belongsTo(Transmission::class);
    }

    public function fuel()
    {
        return $this->belongsTo(Fuel::class);
    }
}
