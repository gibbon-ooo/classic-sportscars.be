<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transmission extends Model
{
    use HasFactory;

    public function getNameAttribute()
    {
        return $this['name_'.session('locale')];
    }
    
    public function cars()
    {
        return $this->hasMany(Car::class);
    }
}
