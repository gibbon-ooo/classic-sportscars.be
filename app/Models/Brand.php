<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use HasFactory;

    public static function boot() {
        parent::boot();

        static::creating(function ($model) {        
            $slug = Str::slug(str_replace('/', '-', $model->name), '-');
            $slugCount = count(Brand::whereRaw("slug REGEXP '^{$slug}(-[0-9]*)?$'")->get() );
            $model->slug = ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
        });

        static::updating(function ($model) {        
            if($model->isDirty('name')){
                $slug = Str::slug(str_replace('/', '-', $model->name), '-');
                $slugCount = count(Brand::whereRaw("slug REGEXP '^{$slug}(-[0-9]*)?$'")->get() );
                $model->slug = ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
            }
        });
    }


    public function cars()
    {
        return $this->hasMany(Car::class);
    }
}
