<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ColorPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny(User $user): bool
    {
       return true;
    }

    public function view(User $user, $model): bool
    {
       return true;
    }

    public function create(User $user): bool
    {
       return false;
    }

    public function update(User $user, $model): bool
    {
       return false;
    }

    public function delete(User $user, $model): bool
    {
       return false;
    }

    public function restore(User $user, $model): bool
    {
       return false;
    }
}
