<?php

namespace App\Http\Controllers;

use DB;
use App;
use Log;
use File;
use Mail;
use Sendy;
use Carbon;
use Session;
use Redirect;
use Validator;
use Storage;

use App\Models\Car;
use App\Models\MailLog;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Debugbar;

class PagesController extends Controller
{	

	public function getHome()
    {
        return view('pages.home');   
    } 
    
    public function getOurVehicles()
    {
	    $cars = Car::where('is_active' ,'=', 1)->get();

        return view('pages.vehicles')->with('cars', $cars);   
    } 
    
    public function getWhoWeAre()
    {
        return view('pages.about-us');   
    } 
    
    public function getBuyingAndSelling()
    {
        return view('pages.buy-and-sell');   
    }
    
    public function getContact()
    {
        return view('pages.contact');   
    } 
        
    public function getVehicle(Request $request, $slug)
    {	
	   			
		$car = Car::where( 'slug_'.session('locale'), '=', $slug)->first();
		
		if ($car == null) {					
			abort(404); 
		} 
		
		return view('pages.vehicle')->with('car', $car); 
    } 
    
    public function getGallery()
    {
        //dd(File::allFiles(public_path('gallery'))[0]::getFilename());
        return view('pages.gallery');   
    } 
    
}