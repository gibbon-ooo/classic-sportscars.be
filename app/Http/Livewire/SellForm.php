<?php

namespace App\Http\Livewire;

use Mail;
use App\Models\MailLog;
use Livewire\Component;
use Livewire\WithFileUploads;

class SellForm extends Component
{
    use WithFileUploads;

    public $firstname;
    public $lastname;
    public $email;
    public $telephone;
    public $brand;
    public $description;
    public $website;
    public $file;

    protected $rules = [
        'firstname' => 'required|min:2|max:255',
        'lastname' => 'required|min:2|max:255',
        'email' => 'required|email',
        'telephone' => 'required|max:255',
        'brand' => 'required|max:255',
        'description' => 'required|min:2',
        'website' => 'nullable|url',
        'file' => 'nullable|mimes:png,jpg,jpeg|max:8096',
    ];

    public function submit() {

        $this->validate();

        $messageText = "";
        $messageText .= "<p><strong>First name: </strong>". $this->firstname ."</p>";
        $messageText .= "<p><strong>Last name: </strong>". $this->lastname ."</p>";
        $messageText .= "<p><strong>E-mail: </strong>". $this->email ."</p>";
        $messageText .= "<p><strong>Phone: </strong>". $this->telephone ."</p>";
        $messageText .= "<br>";
        $messageText .= "<p><strong>Marque: </strong>". $this->brand ."</p>";
        $messageText .= "<p><strong>Déscription: </strong>". $this->description ."</p>";
        $messageText .= "<p><strong>Site web: </strong>". $this->website ."</p>";

        $data = array(
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'email' => $this->email,
            'telephone' => $this->telephone,
            'brand' => $this->brand,
            'description' => $this->description,
            'website' => $this->website,
            'messageText' => $messageText,
            'file' => $this->file
        );   

        // Send Mail
        Mail::to("ben.ectors@mac.com")->send(new \App\Mail\SellForm($data));
    
        // Save mailLog
        $mailLog = new MailLog();
        $mailLog->mail_type = "Reprise";
        $mailLog->mail_to = "info@classic-sportscars.be";
        $mailLog->mail_from = $data['email'];
        $mailLog->mail_subject = "Reprise de véhicule";;
        $mailLog->mail_message = $messageText; 
        $mailLog->save();
        
        session()->flash('status', trans('messages.message-sent') );
    }

    public function render()
    {
        return view('livewire.sell-form');
    }
}
