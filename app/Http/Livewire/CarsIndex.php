<?php

namespace App\Http\Livewire;

use App\Models\Car;
use Livewire\Component;

class CarsIndex extends Component
{
    public function render()
    {
        $cars = Car::where('is_featured', 1);
        return view('livewire.cars-index')->with('cars', $cars);
    }
}
