<?php

namespace App\Http\Livewire;

use App\Models\Car;
use Livewire\Component;
use App\Http\Livewire\DataTable\WithSorting;
use App\Http\Livewire\DataTable\WithCachedRows;

class CarsFilter extends Component
{
    use WithSorting, WithCachedRows;

    public $search;
    public $brand;
    public $fuel;
    public $vatRecoverable;

    protected $queryString = [
        'search' => ['except' => '', 'as' => 's'],
        'brand' => ['except' => '', 'as' => 'b'],
        'fuel' => ['except' => '', 'as' => 'f'],
        'vatRecoverable'  => ['except' => '', 'as' => 'v'],
    ];

    public function updatedFilters()
    {
        //$this->resetPage();
    }

    public function resetFilters()
    {
        $this->reset('search','brand','fuel','vatRecoverable');
    }

    public function getRowsQueryProperty()
    {
        $query = Car::query()
            ->when($this->brand, fn($query, $brandId) => $query->where('brand_id', $brandId))
            ->when($this->fuel, fn($query, $fuelId) => $query->where('fuel_id', $fuelId))
            ->when($this->vatRecoverable, fn($query, $vatRecoverable) => $query->where('is_vat_vehicle', 1))
            ->when($this->search, fn($query, $search) => $query->where('title_nl', 'like', '%'.$search.'%')->orWhere('title_fr', 'like', '%'.$search.'%')->orWhere('title_en', 'like', '%'.$search.'%'));

        return $this->applySorting($query);
    }

    public function getRowsProperty()
    {
        return $this->cache(function () {
            return $this->rowsQuery->get();
        });
    }

    public function render()
    {
        return view('livewire.cars-filter', [
            'cars' => $this->rows,
        ]);
    }
}
