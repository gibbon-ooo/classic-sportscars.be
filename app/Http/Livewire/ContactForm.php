<?php

namespace App\Http\Livewire;

use Mail;
use App\Models\MailLog;
use Livewire\Component;

class ContactForm extends Component
{
    public $firstname;
    public $lastname;
    public $email;
    public $telephone;
    public $subject;
    public $message;

    protected $rules = [
        'firstname' => 'required|min:2|max:255',
        'lastname' => 'required|min:2|max:255',
        'email' => 'required|email',
        'telephone' => 'required|max:255',
        'subject' => 'required|min:2|max:255',
        'message' => 'required|min:2',
    ];

    public function submit() {

        $this->validate();

        $messageText = "";
        $messageText .= "<p><strong>First name: </strong>". $this->firstname ."</p>";
        $messageText .= "<p><strong>Last name: </strong>". $this->lastname ."</p>";
        $messageText .= "<p><strong>E-mail: </strong>". $this->email ."</p>";
        $messageText .= "<p><strong>Phone: </strong>". $this->telephone ."</p>";
        $messageText .= "<br>";
        $messageText .= "<p><strong>Subject: </strong>". $this->subject ."</p>";
        $messageText .= "<p><strong>Message: </strong>". $this->message ."</p>";

        $data = array(
            'firstname'     => $this->firstname,
            'lastname'      => $this->lastname,
            'telephone'     => $this->telephone,
            'email'         => $this->email,
            'messageText'   => $this->message,
            'subject'       => $this->subject,
        );

        // Send Mail
        Mail::to("ben.ectors@mac.com")->send(new \App\Mail\ContactForm($data));
 
         // Save mailLog
        $mailLog = new MailLog();
        $mailLog->mail_type = "Contact";
        $mailLog->mail_to = "info@classic-sportscars.be";
        $mailLog->mail_from = $data['email'];
        $mailLog->mail_subject = $this->subject;
        $mailLog->mail_message = $messageText; 
        $mailLog->save();
        
        session()->flash('status', trans('messages.message-sent') );
    }

    public function render()
    {
        return view('livewire.contact-form');
    }
}
