<?php

use App\Http\Controllers\PagesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::localizedGroup(function () {
    Route::transGet('/', [PagesController::class, 'getHome']);
    Route::transGet('routes.home', [PagesController::class, 'getHome']);
    Route::transGet('routes.our-vehicles', [PagesController::class, 'getOurVehicles']);
    Route::transGet('routes.our-vehicles/car', [PagesController::class, 'getVehicle']);       
    Route::transGet('routes.who-we-are', [PagesController::class, 'getWhoWeAre']);
    Route::transGet('routes.buying-and-selling', [PagesController::class, 'getBuyingAndSelling']);
    Route::transPost('routes.buying-and-selling', [PagesController::class, 'postBuyingAndSelling']);
    Route::transGet('routes.contact', [PagesController::class, 'getContact']);
    Route::transPost('routes.contact', [PagesController::class, 'postContact']);
    Route::transGet('routes.gallery', [PagesController::class, 'getGallery']);
});
